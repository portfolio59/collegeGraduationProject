package tycoongraduate.run;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import tycoongraduate.views.MainFrame;

public class Run {
	
	public static void main(String[] args) {
		new MainFrame();
	}

}

/*
 * JLabel(new ImageIcon)
 * JButton(new ImageIcon)
 * 
 * paint(g){
 * g.drawImage(new Image)
 * }
 * 위 방식보다 라벨이용해서  아래처럼 이용하는게 겹치지 않을 것이다
 * Image cimage = character.characterMpngbf().getImage().getScaledIn2stance(250, 250, 0);
 * ImageIcon cIcon = new ImageIcon(cimage);
 * 위에는 비효율적인 코드! 아래가 길어도 깔끔!
 * 
 * - 트로트게임 세컨드 패널 클래스에 있는거
 * getScaledInstance() 에서 3번째 인자는 이미지 크기 변환을 했을 때 이미지의 품질을 그대로 유지시켜준다
 * totalScoreLabel = new JLabel(new ImageIcon(dialog.talkFormat().getImage().getScaledInstance(1400, 400,java.awt.Image.SCALE_SMOOTH)));
 * 
 * JLabel cLabel = new JLabel(new ImageIcon(character.characterMpngbf().getImage().getScaledInstance(250, 250, 0)));
 * panel.add(cLabel);
 * 
 * JLabel.setIcon(new ImageIcon)
 * 
 */

