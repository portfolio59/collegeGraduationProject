package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class CharacterImage {
	private ImageIcon characterMgifbf;
	private ImageIcon characterMpngbf;
	private ImageIcon characterWgifbf;
	private ImageIcon characterWpngbf;
	private ImageIcon characterMgifcy;
	private ImageIcon characterWgifcy;
	
	private ImageIcon characterMgifaf;
	private ImageIcon characterMpngaf;
	private ImageIcon characterWgifaf;
	private ImageIcon characterWpngaf;
	private ImageIcon endingCharacterMgif;
	private ImageIcon endingCharacterWgif;
	
	private ImageIcon boss;
	private ImageIcon bossMark;
	
	//추리닝 남자 동적 gif 이미지 = images/chracters/CharacterM.gif
	public ImageIcon characterMgifbf() {
		characterMgifbf = new ImageIcon("src/tycoongraduate/images/chracters/CharacterM.gif");
		return characterMgifbf;
	}
	//추리닝 남자 정적 png 이미지 = images/chracters/CharacterM.png
	public ImageIcon characterMpngbf() {
		characterMpngbf = new ImageIcon("src/tycoongraduate/images/chracters/CharacterM.png");
		return characterMpngbf;
	}
	//우는 남자아이 상반신 gif 이미지 = images/characters/Lose_CharacterM.gif
	public ImageIcon characterMgifcy() {
		characterMgifcy = new ImageIcon("src/tycoongraduate/images/chracters/Lose_CharacterM.gif");
		return characterMgifcy;
	}
	//우는 여자아이 상반신 gif 이미지 = images/characters/Lose_CharacterM.gif
		public ImageIcon characterWgifcy() {
			characterWgifcy = new ImageIcon("src/tycoongraduate/images/chracters/Lose_CharacterW.gif");
			return characterWgifcy;
		}
	//추리닝 여자 동적 gif 이미지 = images/chracters/CharacterW.gif
	public ImageIcon characterWgifbf() {
		characterWgifbf = new ImageIcon("src/tycoongraduate/images/chracters/CharacterW.gif");
		return characterWgifbf;
	}
	//추리닝 여자 정적 png 이미지 = images/chracters/CharacterW.png
	public ImageIcon characterWpngbf() {
		characterWpngbf = new ImageIcon("src/tycoongraduate/images/chracters/CharacterW.png");
		return characterWpngbf;
	}
	//변신후 남자 동적 gif 이미지 = images/chracters/FCharacterM.gif
	public ImageIcon characterMgifaf() {
		characterMgifaf = new ImageIcon("src/tycoongraduate/images/chracters/FCharacterM.gif");
		return characterMgifaf;
	}
	//변신후 남자 정적 png 이미지 = images/chracters/FCharacterM.png
	public ImageIcon characterMpngaf() {
		characterMpngaf = new ImageIcon("src/tycoongraduate/images/chracters/FCharacterM.png");
		return characterMpngaf;
	}
	//변신후 여자 동적 gif 이미지 = images/chracters/FCharacterW.gif
	public ImageIcon characterWgifaf() {
		characterWgifaf = new ImageIcon("src/tycoongraduate/images/chracters/FCharacterW.gif");
		return characterWgifaf;
	}
	//변신후 여자 정적 png 이미지 = images/chracters/FCharacterW.png
	public ImageIcon characterWpngaf() {
		characterWpngaf = new ImageIcon("src/tycoongraduate/images/chracters/FCharacterW.png");
		return characterWpngaf;
	}
	//엔딩 남자 동적 gif 이미지 = images/chracters/EndingCharacterM.gif
	public ImageIcon endingCharacterMgif() {
		endingCharacterMgif = new ImageIcon("src/tycoongraduate/images/chracters/EndingCharacterM.gif");
		return endingCharacterMgif;
	}
	//엔딩 여자 동적 gif 이미지 = images/chracters/EndingCharacterW.gif
	public ImageIcon endingCharacterWgif() {
		endingCharacterWgif = new ImageIcon("src/tycoongraduate/images/chracters/EndingCharacterW.gif");
		return endingCharacterWgif;
	}
	//소속사 사장 정적 = images/chracters/boss.png
	public ImageIcon boss() {
		boss = new ImageIcon("src/tycoongraduate/images/chracters/boss.png");
		return boss;
	}
	//느낌표 있는 소속사 사장 정적 = images/chracters/boss2.png
	public ImageIcon bossMark() {
		bossMark = new ImageIcon("src/tycoongraduate/images/chracters/boss2.png");
		return bossMark;
	}
}
