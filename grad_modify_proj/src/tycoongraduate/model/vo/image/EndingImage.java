package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class EndingImage {
	private ImageIcon ending_sudden;
	private ImageIcon ending_top;
	private ImageIcon ending_middle;
	private ImageIcon ending_bottom;
	
	public EndingImage() {}
	
	//돌발미션 실패 엔딩 이미지 = images/ending/ending01.png
	public ImageIcon ending_sudden() {
		ending_sudden = new ImageIcon("src/tycoongraduate/images/ending/ending01.png");
		return ending_sudden;
	}
	//월드 스타 엔딩 이미지 = images/ending/ending03.png
	public ImageIcon ending_top() {
		ending_top = new ImageIcon("src/tycoongraduate/images/ending/ending03.png");
		return ending_top;
	}
	//무명 스타 엔딩 이미지 = images/ending/endingmo.png
	public ImageIcon ending_middle() {
		ending_middle = new ImageIcon("src/tycoongraduate/images/ending/endingmo.png");
		return ending_middle;
	}
	//방구석 스타 엔딩 이미지 = images/ending/ending02.png
	public ImageIcon ending_bottom() {
		ending_bottom = new ImageIcon("src/tycoongraduate/images/ending/ending02.png");
		return ending_bottom;
	}
		
}
