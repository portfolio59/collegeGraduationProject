package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class MapImage {
	
	private ImageIcon mainMap;
	private ImageIcon parkMap;
	private ImageIcon park2Map;
	private ImageIcon parkWMap;
	private ImageIcon parkW2Map;
	private ImageIcon compMap;
	private ImageIcon mark;
	private ImageIcon que1;
	private ImageIcon que2;
	private ImageIcon que3;
	private ImageIcon que4;
	
	public MapImage() {}
	
	//메인도로 화면 = images/mainMap/MainMap.png
	public ImageIcon mainMap() {
		mainMap = new ImageIcon("src/tycoongraduate/images/mainMap/MainMap.png");
		return mainMap;
	}
	//공원에 남자 혼자 있는 배경 = images/parkMap/park.png
	public ImageIcon parkMap() {
		parkMap = new ImageIcon("src/tycoongraduate/images/parkMap/park.png");
		return parkMap;
	}
	//공원에 남자 둘이 있는 배경 = images/parkMap/park2.png
	public ImageIcon park2Map() {
		park2Map = new ImageIcon("src/tycoongraduate/images/parkMap/park2.png");
		return park2Map;
	}
	//공원에 여자 혼자 있는 배경 = images/parkMap/park.png
	public ImageIcon parkWMap() {
		parkWMap = new ImageIcon("src/tycoongraduate/images/parkMap/parkW.png");
		return parkWMap;
	}
	//공원에 여자 한명 남자 한명이 있는 배경 = images/parkMap/park2.png
	public ImageIcon parkW2Map() {
		parkW2Map = new ImageIcon("src/tycoongraduate/images/parkMap/parkW2.png");
		return parkW2Map;
	}
	//소속사 배경 이미지 = images/companyMap/comp_background.png
	public ImageIcon compMap() {
		compMap = new ImageIcon("src/tycoongraduate/images/companyMap/comp_background.png");
		return compMap;
	}
	//느낌표 = images/parkMap/exMark.png
	public ImageIcon mark() {
		mark = new ImageIcon("src/tycoongraduate/images/parkMap/exMark.png");
		return mark;
	}
	//작은 퀘스트 화면 = images/Que1.png
	public ImageIcon que1() {
		que1 = new ImageIcon("src/tycoongraduate/images/Que1.png");
		return que1;
	}
	//작은 퀘스트 화면 = images/Que2.png
	public ImageIcon que2() {
		que2 = new ImageIcon("src/tycoongraduate/images/Que2.png");
		return que2;
	}
	//작은 퀘스트 화면 = images/Que3.png
		public ImageIcon que3() {
			que3 = new ImageIcon("src/tycoongraduate/images/Que3.png");
			return que3;
		}
	//작은 퀘스트 화면 = images/Que4.png
	public ImageIcon que4() {
		que4 = new ImageIcon("src/tycoongraduate/images/Que4.png");
		return que4;
	}
}
