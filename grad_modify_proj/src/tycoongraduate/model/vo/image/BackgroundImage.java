package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class BackgroundImage {
	private ImageIcon firstScreen;
	private ImageIcon characterSelecBg;
	private ImageIcon redBground;
	private ImageIcon mainRoom;
	private ImageIcon greyRoomBg;
    private ImageIcon outDoor;
    private ImageIcon bigDoor;
    private ImageIcon singBground;
    private ImageIcon singBgroundB;
    private ImageIcon jungukBground;
    private ImageIcon trotBground;
    private ImageIcon trotOnCamera;
    private ImageIcon trotOffCamera;
    private ImageIcon suddenBground;
    
	public BackgroundImage() {}
	
	//마이크있고 게임화면 첫시작
	public ImageIcon firstScreen() {
		firstScreen = new ImageIcon("src/tycoongraduate/images/firstScreen.gif");
		return firstScreen;
	}
	//케릭터 선택 화면에서 빨간커텐 배경(글자 추가)
	public ImageIcon characterSelecBg() {
		characterSelecBg = new ImageIcon("src/tycoongraduate/images/characterSelecBg.png");
		return characterSelecBg;
	}
	//빨간커텐 배경
		public ImageIcon redBground() {
			redBground = new ImageIcon("src/tycoongraduate/images/redCuttonBackground.jpeg");
			return redBground;
		}
	//처음 플레이어 방안 배경 (회색) 이미지
	public ImageIcon greyRoomBg() {
		greyRoomBg = new ImageIcon("src/tycoongraduate/images/greyImage.png");
		return greyRoomBg;
	}
	//처음 플레이어 방안 이미지
	public ImageIcon mainRoom() {
		mainRoom = new ImageIcon("src/tycoongraduate/images/playerRoom.jpg");
		return mainRoom;
	}
	//바깥 문 (장식) = images/companyMap/outdoor.png
	public ImageIcon outDoor() {
		outDoor = new ImageIcon("src/tycoongraduate/images/companyMap/outdoor.png");
		return outDoor;
	}
	//큰 문 (장식) = images/companyMap/door.png
	public ImageIcon bigDoor() {
		bigDoor = new ImageIcon("src/tycoongraduate/images/companyMap/door.png");
		return bigDoor;
	}
	//노래연습 배경 이미지 - images/companyMap/ingame_background.png
	public ImageIcon singBground() {
		singBground = new ImageIcon("src/tycoongraduate/images/companyMap/ingame_background.png");
		return singBground;
	}
	//노래연습 배경 이미지 블라인드처리 - images/companyMap/ingame_background_b.png
	public ImageIcon singBgroundB() {
		singBgroundB = new ImageIcon("src/tycoongraduate/images/companyMap/ingame_background_b.png");
		return singBgroundB;
	}
	//전국 노래자랑 배경 이미지 = images/miniGame/junguk_game/junguk_background.png
	public ImageIcon jungukBground() {
		jungukBground = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/junguk_background.png");
		return jungukBground;
	}
	//트롯스타게임 빨간색 배경에 심사위원 이미지 = images/miniGame/camera_game/panel3.png
	public ImageIcon trotBground() {
		trotBground = new ImageIcon("src/tycoongraduate/images/miniGame/camera_game/panel3.png");
		return trotBground;
	}
	//트롯스타게임 카메라 켜진 (장식) 이미지 = images/miniGame/camera_game/image_1.png
	public ImageIcon trotOnCamera() {
		trotOnCamera = new ImageIcon("src/tycoongraduate/images/miniGame/camera_game/image_1.png");
		return trotOnCamera;
	}
	//트롯스타게임 카메라 꺼진 (장식) 심사위원 이미지 = images/miniGame/camera_game/image_0.png
	public ImageIcon trotOffCamera() {
		trotOffCamera = new ImageIcon("src/tycoongraduate/images/miniGame/camera_game/image_0.png");
		return trotOffCamera;
	}
	//주인공이 서있는 배경 이미지 = images/miniGame/sudden_game/bground_0.jpg
	public ImageIcon suddenBground() {
		suddenBground = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/bground_0.jpg");
		return suddenBground;
	}
}
