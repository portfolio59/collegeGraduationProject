package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class DialogImage {
	private ImageIcon dialogTalk;
	private ImageIcon nickn;
	private ImageIcon goToPark;
	private ImageIcon loseDialog;
	private ImageIcon winDialog;
	private ImageIcon jungukDialog1;
	private ImageIcon jungukDialog2;
	private ImageIcon jungukDialogQ1;
	private ImageIcon jungukDialogFail;
	private ImageIcon jungukScoreLabel;
	private ImageIcon trotDialogLabel;
	private ImageIcon suddenDialogStart;
	private ImageIcon suddenDialogBanner;
	private ImageIcon suddenDialogExplain;
	private ImageIcon suddenDialogFail;
	private ImageIcon suddenDialogPass;
	private ImageIcon dispatchDialogExplain;
	
	public DialogImage () {}
	//내용없는 대화창 틀 이미지
	public ImageIcon talkFormat() {
		dialogTalk = new ImageIcon("src/tycoongraduate/images/dialog_format.png");
		return dialogTalk;
	}
	//닉네임 정하는 창에서 닉네임 라벨 이미지
	public ImageIcon nickn() {
		nickn = new ImageIcon("src/tycoongraduate/images/nicknameB.png");
		return nickn;
	}
	//공원을 찾아가라 말풍선 = images/mainMap/park_dialog_inMain.png
	public ImageIcon goToPark() {
		goToPark = new ImageIcon("src/tycoongraduate/images/mainMap/parkQ.png");
		return goToPark;
	}
	//lose 크아 이미지 = images/miniGame/lose.png
	public ImageIcon loseDialog() {
		loseDialog = new ImageIcon("src/tycoongraduate/images/miniGame/lose.png");
		return loseDialog;
	}
	//win 크아 이미지 = images/miniGame/lose.png
	public ImageIcon winDialog() {
		winDialog = new ImageIcon("src/tycoongraduate/images/miniGame/win.png");
		return winDialog;
	}
	//전국 노래자랑 이미지 없는 대화창 내용있음 = images/miniGame/junguk_game/mal_junguk1.PNG
	public ImageIcon jungukDialog1() {
		jungukDialog1 = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/mal_junguk1.PNG");
		return jungukDialog1;
	}
	//전국 노래자랑 이미지 있는 대화창 내용있음 = images/miniGame/junguk_game/junguk_dialog.PNG
	public ImageIcon jungukDialog2() {
		jungukDialog2 = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/junguk_dialog.PNG");
		return jungukDialog2;
	}
	//전국 노래자랑 문제 설명 이미지 = images/miniGame/junguk_game/Q1.png
	public ImageIcon jungukDialogQ1() {
		jungukDialogQ1 = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/Q1.png");
		return jungukDialogQ1;
	}
	//전국 노래자랑 실패 대화창 이미지 = images/miniGame/fail_dialog.png
	public ImageIcon jungukDialogFail() {
		jungukDialogFail = new ImageIcon("src/tycoongraduate/images/miniGame/fail_dialog.png");
		return jungukDialogFail;
	}
	//전국 노래자랑 점 획득 글자 이미지 = images/miniGame/score_label.png
	public ImageIcon jungukScoreLabel() {
		jungukScoreLabel = new ImageIcon("src/tycoongraduate/images/miniGame/score_label.png");
		return jungukScoreLabel;
	}
	//트롯스타게임 설명 이미지 = images/miniGame/camera_game/camera_game_dialog.png
	public ImageIcon trotDialogLabel() {
		trotDialogLabel = new ImageIcon("src/tycoongraduate/images/miniGame/camera_game/camera_game_dialog.png");
		return trotDialogLabel;
	}
	//돌발게임으로 향하는 첫 시작 대화창 이미지 = images/miniGame/sudden_game/start.png
	public ImageIcon suddenDialogStart() {
		suddenDialogStart = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/start.png");
		return suddenDialogStart;
	}
	//돌발게임으로 향하는 첫 시작 대화창 배너 이미지 = images/miniGame/sudden_game/banner.png
		public ImageIcon suddenDialogBanner() {
			suddenDialogBanner = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/banner.png");
			return suddenDialogBanner;
		}
	//돌발 게임 설명 이미지 = images/miniGame/sudden_game/info_60.jpg
	public ImageIcon suddenDialogExplain() {
		suddenDialogExplain = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/info_60.jpg");
		return suddenDialogExplain;
	}
	//돌발 게임 실패 이미지 = images/suddenGame/fail.png
	public ImageIcon suddenDialogFail() {
		suddenDialogFail = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_Game/fail.png");
		return suddenDialogFail;
	}
	//돌발 게임 통과 이미지 = images/suddenGame/pass.png
	public ImageIcon suddenDialogPass() {
		suddenDialogPass = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_Game/pass.png");
		return suddenDialogPass;
	}
	//디스패치 게임 설명 이미지 = images/miniGame/dispatch_game/dispatch_game_dialog.png
	public ImageIcon dispatchDialogExplain() {
		dispatchDialogExplain = new ImageIcon("src/tycoongraduate/images/miniGame/dispatch_game/dispatch_game_dialog.png");
		return dispatchDialogExplain;
	}
}
