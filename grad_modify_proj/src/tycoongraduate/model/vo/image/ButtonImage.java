package tycoongraduate.model.vo.image;

import javax.swing.ImageIcon;

public class ButtonImage {
	private ImageIcon select_btn;
	private ImageIcon submit_btn;
	private ImageIcon start_btn;
	private ImageIcon back_btn;
	private ImageIcon home_btn;
	private ImageIcon replay_btn;
	private ImageIcon hallOfFame_btn;
	
	public ButtonImage() {}
	//select 버튼 이미지 = images/select.png
	public ImageIcon select_btn() {
		select_btn = new ImageIcon("src/tycoongraduate/images/select.png");
		return select_btn;
	}
	//submit 버튼 이미지 = images/submit.png
	public ImageIcon submit_btn() {
		submit_btn = new ImageIcon("src/tycoongraduate/images/submit.png");
		return submit_btn;
	}
	//start 버튼 이미지 = images/start.png
	public ImageIcon start_btn() {
		start_btn = new ImageIcon("src/tycoongraduate/images/start.png");
		return start_btn;
	}
	//back 버튼 이미지 = images/back.png
	public ImageIcon back_btn() {
		back_btn = new ImageIcon("src/tycoongraduate/images/back.png");
		return back_btn;
	}
	//back 버튼 이미지 = images/home.png
	public ImageIcon home_btn() {
		home_btn = new ImageIcon("src/tycoongraduate/images/home.png");
		return home_btn;
	}
	//back 버튼 이미지 = images/replay.png
	public ImageIcon replay_btn() {
		replay_btn = new ImageIcon("src/tycoongraduate/images/replay.png");
		return replay_btn;
	}
	//back 버튼 이미지 = images/hallOfFame.png
	public ImageIcon hallOfFame_btn() {
		hallOfFame_btn = new ImageIcon("src/tycoongraduate/images/hallOfFame.png");
		return hallOfFame_btn;
	}
}
