package tycoongraduate.model.vo.game;

import javax.swing.ImageIcon;

public class JungukGameVo {
	private ImageIcon time0;
	private ImageIcon time10;
	private ImageIcon time20;
	private ImageIcon time30;
	private ImageIcon time40;
	private ImageIcon time50;
	private ImageIcon time60;
	
	private ImageIcon quest;
	
	public JungukGameVo() {}
	//전국 노래자랑 타이머 0초 이미지 = images/miniGame/0.PNG
	public ImageIcon time0() {
		time0 = new ImageIcon("src/tycoongraduate/images/miniGame/0.PNG");
		return time0;
	}
	//전국 노래자랑 타이머 10초 이미지 = images/miniGame/10.PNG
	public ImageIcon time10() {
		time10 = new ImageIcon("src/tycoongraduate/images/miniGame/10.PNG");
		return time10;
	}
	//전국 노래자랑 타이머 20초 이미지 = images/miniGame/20.PNG
	public ImageIcon time20() {
		time20 = new ImageIcon("src/tycoongraduate/images/miniGame/20.PNG");
		return time20;
	}
	//전국 노래자랑 타이머 30초 이미지 = images/miniGame/30.PNG
	public ImageIcon time30() {
		time30 = new ImageIcon("src/tycoongraduate/images/miniGame/30.PNG");
		return time30;
	}
	//전국 노래자랑 타이머 40초 이미지 = images/miniGame/40.PNG
	public ImageIcon time40() {
		time40 = new ImageIcon("src/tycoongraduate/images/miniGame/40.PNG");
		return time40;
	}
	//전국 노래자랑 타이머 50초 이미지 = images/miniGame/50.PNG
	public ImageIcon time50() {
		time50 = new ImageIcon("src/tycoongraduate/images/miniGame/50.PNG");
		return time50;
	}
	//전국 노래자랑 타이머 60초 이미지 = images/miniGame/60.PNG
	public ImageIcon time60() {
		time60 = new ImageIcon("src/tycoongraduate/images/miniGame/60.PNG");
		return time60;
	}
	//랜덤으로 숫자를 받아서 그에 해당하는 문제 이미지 출력
	public ImageIcon quest(int num) {
		switch(num) {
		case 0: quest = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/0_game.png"); break;
		case 1: quest = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/1_game.png"); break;
		case 2: quest = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/2_game.png"); break;
		case 3: quest = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/3_game.png"); break;
		case 4: quest = new ImageIcon("src/tycoongraduate/images/miniGame/junguk_game/4_game.png"); break;
		}
		return quest;
	}
}
