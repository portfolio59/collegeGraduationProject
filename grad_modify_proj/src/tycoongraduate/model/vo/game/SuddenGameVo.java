package tycoongraduate.model.vo.game;

import javax.swing.ImageIcon;

public class SuddenGameVo {
	private ImageIcon puzzle;
	
	public SuddenGameVo() {}
	
	//돌발 게임 퍼즐 이미지 1~9 = images/miniGame/sudden_game/1.jpg ~ images/miniGame/sudden_game/9.jpg
	public ImageIcon puzzleImg(int num) {
		switch(num) {
		case 1: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/1.jpg"); break;
		case 2: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/2.jpg"); break;
		case 3: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/3.jpg"); break;
		case 4: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/4.jpg"); break;
		case 5: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/5.jpg"); break;
		case 6: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/6.jpg"); break;
		case 7: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/7.jpg"); break;
		case 8: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/8.jpg"); break;
		case 9: puzzle = new ImageIcon("src/tycoongraduate/images/miniGame/sudden_game/9.jpg"); break;
		}
		return puzzle;
	}
}