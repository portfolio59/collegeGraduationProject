package tycoongraduate.model.vo.game;

import javax.swing.ImageIcon;

public class DispatchGameImagesVo {
	private ImageIcon scoreAvoidImg;
	private ImageIcon scoreHeartImg;
	private ImageIcon scoreStarImg;
	private ImageIcon dispatchGameBgImg;
	
	public DispatchGameImagesVo () {}
	
	//디스패치 게임 배경화면 이미지 = images/miniGame/dispatch_game/background.png
	public ImageIcon dispatchGameBgImg() {
		dispatchGameBgImg = new ImageIcon("src/tycoongraduate/images/miniGame/dispatch_game/background.png");
		return dispatchGameBgImg;
	}
	//디스패치 게임 피하기(장식) 이미지 = images/miniGame/dispatch_game/score_avoid.png
	public ImageIcon scoreAvoidImg() {
		scoreAvoidImg = new ImageIcon("src/tycoongraduate/images/miniGame/dispatch_game/score_avoid.png");
		return scoreAvoidImg;
	}
	//디스패치 게임 하트먹기(장식) 이미지 = images/miniGame/dispatch_game/score_heart.png
	public ImageIcon scoreHeartImg() {
		scoreHeartImg = new ImageIcon("src/tycoongraduate/images/miniGame/dispatch_game/score_heart.png");
		return scoreHeartImg;
	}
	//디스패치 게임 별먹기(장식) 이미지 = images/miniGame/dispatch_game/score_star.png
	public ImageIcon scoreStarImg() {
		scoreStarImg = new ImageIcon("src/tycoongraduate/images/miniGame/dispatch_game/score_star.png");
		return scoreStarImg;
	}
}
