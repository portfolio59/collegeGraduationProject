package tycoongraduate.model.vo.game;

public class DispatchGameScoreHeartVo {
	int x;
	int y;
	int speed;

	int score;
	int total;

	public DispatchGameScoreHeartVo() {
		super();
	}
	public DispatchGameScoreHeartVo(int x, int y, int speed) {
		this.x = x;
		this.y = y;
		this.speed = speed;
	}
	public void move() {
		x -= speed ;      
	}
	
	//getter, setter 부분
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

}