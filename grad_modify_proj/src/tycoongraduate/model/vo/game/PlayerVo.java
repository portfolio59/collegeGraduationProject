package tycoongraduate.model.vo.game;

import java.io.Serializable;
import java.sql.Date;

public class PlayerVo implements Serializable{
	private String userId;
	private String gender;
	private int totalScore;
	private int danceScore;
	private String endingComment;
	private Date endingDate;
	private int singScore;
	private int playerX = 0;
	private int playerY = 0;
	//플레이어의 퀘스트 진행 상태를 나타내는 변수, 각각 진행을 했으면 +1씩 증가됨
	//1 : 퀘스트 1진행상태, 2: 퀘스트 2 진행상태, 3: 퀘스트 3 진행상태, 4: 퀘스트 4 진행상태
	private int quest;

	public PlayerVo() {}

	public PlayerVo(String userId, String gender, int totalScore, int danceScore, String endingComment, Date endingDate,
			int singScore, int playerX, int playerY, int quest) {
		super();
		this.userId = userId;
		this.gender = gender;
		this.totalScore = totalScore;
		this.danceScore = danceScore;
		this.endingComment = endingComment;
		this.endingDate = endingDate;
		this.singScore = singScore;
		this.playerX = playerX;
		this.playerY = playerY;
		this.quest = quest;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public int getDanceScore() {
		return danceScore;
	}

	public void setDanceScore(int danceScore) {
		this.danceScore = danceScore;
	}

	public String getEndingComment() {
		return endingComment;
	}

	public void setEndingComment(String endingComment) {
		this.endingComment = endingComment;
	}

	public Date getEndingDate() {
		return endingDate;
	}

	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}

	public int getSingScore() {
		return singScore;
	}

	public void setSingScore(int singScore) {
		this.singScore = singScore;
	}

	public int getPlayerX() {
		return playerX;
	}

	public void setPlayerX(int playerX) {
		this.playerX = playerX;
	}

	public int getPlayerY() {
		return playerY;
	}

	public void setPlayerY(int playerY) {
		this.playerY = playerY;
	}

	public int getQuest() {
		return quest;
	}

	public void setQuest(int quest) {
		this.quest = quest;
	}
}