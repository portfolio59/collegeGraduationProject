package tycoongraduate.model.dao;

import static tycoongraduate.model.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import tycoongraduate.model.vo.game.PlayerVo;

public class UserDao {
	private Properties prop = new Properties();

	public UserDao() {
		String fileName = UserDao.class.getResource("/sql/user/user-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public int insertUser(Connection con, PlayerVo player) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertUser");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, player.getUserId());
			pstmt.setString(2, player.getGender());
			pstmt.setInt(3, player.getTotalScore());
			pstmt.setInt(4, player.getDanceScore());
			pstmt.setInt(5, player.getSingScore());
			pstmt.setString(6, player.getEndingComment());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	public ArrayList<PlayerVo> selectUser(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<PlayerVo> list = null;
		
		String query = prop.getProperty("selectUser");
		
		try {
			stmt = con.createStatement();
		
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				PlayerVo selectUser = new PlayerVo();
				
				selectUser.setUserId(rset.getString("USER_ID"));
				selectUser.setGender(rset.getString("GENDER"));
				selectUser.setTotalScore(rset.getInt("TOTAL_SCORE"));
				selectUser.setDanceScore(rset.getInt("DANCE_SCORE"));
				selectUser.setSingScore(rset.getInt("SING_SCORE"));
				selectUser.setEndingComment(rset.getString("ENDING_COMMENT"));
				selectUser.setEndingDate(rset.getDate("ENDING_DATE"));
				
				System.out.println("selectUser 조회된 유저 : "+ selectUser.getUserId());
				list.add(selectUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		return list;
	}
}
