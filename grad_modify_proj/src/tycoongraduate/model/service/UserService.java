package tycoongraduate.model.service;

import static tycoongraduate.model.common.JDBCTemplate.close;
import static tycoongraduate.model.common.JDBCTemplate.commit;
import static tycoongraduate.model.common.JDBCTemplate.getConnection;
import static tycoongraduate.model.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import tycoongraduate.model.dao.UserDao;
import tycoongraduate.model.vo.game.PlayerVo;

public class UserService {
	private Connection con;
	public void insertUser(PlayerVo player) {
		con = getConnection();
		int insertUser = new UserDao().insertUser(con, player);

		if(insertUser > 0) {
			commit(con);
			System.out.println("유저 DB 등록 성공");
		} else {
			rollback(con);
			System.out.println("유저 DB 등록 실패");
		}
		close(con);
	}

	public ArrayList<PlayerVo> selectUser(PlayerVo player) {
		con = getConnection();
		
		ArrayList<PlayerVo> list = new UserDao().selectUser(con);
		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}
		close(con);
		
		return list;
	}
}