package tycoongraduate.model.controller;

import java.util.ArrayList;

import tycoongraduate.model.service.UserService;
import tycoongraduate.model.vo.game.PlayerVo;

public class UserController {
	private UserService userService;
	
	//유저 정보 DB 저장
	public void userStoredDb(PlayerVo player) {
		userService = new UserService();
		userService.insertUser(player);
	}
	//명예의전당에서 출력할 유저 정보 조회
	public ArrayList<PlayerVo> selectAllUser(PlayerVo player) {
		return new UserService().selectUser(player);
	}
}
