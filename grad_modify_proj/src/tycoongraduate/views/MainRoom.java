package tycoongraduate.views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.common.PlayerLabel;
import tycoongraduate.views.common.QuestInfor;

public class MainRoom extends JPanel{
	private MainFrame mf;
	private MainRoom panel;
	private PlayerVo player;
	private PlayerLabel playerLabel;
	private QuestInfor questInfor;
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	public MainRoom(MainFrame mf, PlayerVo player) {
		System.out.println("MainRoom class..player.getUserId : " + player.getUserId());
		questInfor = new QuestInfor(player);
		panel = this;
		this.mf = mf;
		this.player = player;
		System.out.println("MainRoom class..player.getGender : " + player.getGender());
		playerLabel = new PlayerLabel(player);
		questInfor.setLayout(null);
		questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		panel.setLayout(null);
		playerLabel.setLayout(null);
		playerLabel.setBounds(650,320,100,100);
		this.add(playerLabel);
		this.setVisible(true);
		addKeyListener(new MyKeyListenerMainRoom(playerLabel,mf,panel,player));
	}
	public void paintComponent(Graphics g) {
		setFocusable(true);
		requestFocus();
		Font font2 = new Font("Neo둥근모", Font.BOLD, 40);
		super.paintComponent(g);
		g.drawImage(bg.greyRoomBg().getImage(), 0, 0, 1400, 900, null);
		//처음 플레이어 방안 이미지 = images/playerRoom.jpg
		g.drawImage(bg.mainRoom().getImage(),250,120,848,482,null);
		//말풍선 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(),5,620,1390,250,null);
		g.setFont(font2);
		g.drawString("날씨가 좋아서 공원에 가고 싶은 날이야. 한번 나가볼까?", 70, 750);
		setOpaque(false);
	}
}