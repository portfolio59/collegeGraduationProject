package tycoongraduate.views.common;

import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class Timer extends Thread{

	private JPanel panel;

	public Timer(JPanel panel){
		this.panel = panel;
	}
	@SuppressWarnings("static-access")
	@Override
	public void run() {
		JTextField label = new JTextField("제한 시간 : ");
		label.setBounds(100, 380, 300, 100);
		label.setFont(new Font("Sanscerif", Font.BOLD, 40));
		panel.add(label);
		for(int i = 60; i >= 0; i--){
			try {
				System.out.println(i);
				this.sleep(1000);
				label.setText("남은 시간: " + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


