package tycoongraduate.views.common;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.CharacterImage;

public class PlayerLabel extends JLabel{
	private CharacterImage character = new CharacterImage();
	//추리닝 남자 gif = images/chracters/CharacterM.gif
	public PlayerLabel(PlayerVo player) {
		this.setLocation(440, 440);
		this.setSize(100, 100);
		if(player.getGender() == "M" && player.getQuest() < 4) {
			this.setIcon(new ImageIcon(character.characterMgifbf().getImage().getScaledInstance(100, 100, 0)));
		} else if(player.getGender() == "W" && player.getQuest() < 4){
			this.setIcon(new ImageIcon(character.characterWgifbf().getImage().getScaledInstance(100, 100, 0)));
		} else if(player.getGender() == "M" && player.getQuest() >= 4) {
			this.setIcon(new ImageIcon(character.characterMgifaf().getImage().getScaledInstance(100, 100, 0)));
		} else if(player.getGender() == "W" && player.getQuest() >= 4) {
			this.setIcon(new ImageIcon(character.characterWgifaf().getImage().getScaledInstance(100, 100, 0)));
		}
	}
}