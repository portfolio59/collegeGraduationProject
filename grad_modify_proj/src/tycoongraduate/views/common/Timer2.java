package tycoongraduate.views.common;

import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.singGame.MiniGameSsing_Lose;

public class Timer2 extends Thread{
	private  MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	private int score;
	private int cnt;
	
	public Timer2() {
	}
	
	public Timer2(MainFrame mf, JPanel panel, PlayerVo player) {
		this.mf= mf;
		this.panel =panel;
		this.player = player;
	}
	@SuppressWarnings("static-access")
	@Override
	public void run() {
		int i  = 20;
		JTextField label = new JTextField();
		label.setBounds(0,0,150,50);
		label.setFont(new Font("Neo둥근모",Font.BOLD,20));
		panel.add(label);
		//스레드에 interrupte가 안걸렸을때
		while(i >= 0 && !Thread.currentThread().isInterrupted()) {
			try {
				System.out.println(i);
				this.sleep(1000);
				label.setText("남은시간 : " + i);
			}catch(InterruptedException e) {
				
			}
			i--;
		}
		ssingNext_lose();
		System.out.println("타이머 실행 = Timer2클래스");
	}
	@SuppressWarnings("deprecation")
	public void ssingNext_lose() {
		MiniGameSsing_Lose gp = new MiniGameSsing_Lose(mf,player,score, cnt);
		ChangePanel.changePanel(mf, panel, gp);
		this.stop();
	}
	public class Test1{
		public void process() {
			Timer2 t2 = new Timer2();
			Thread thread = new Thread(t2);
			thread.start();
			try {
				Thread.sleep(2000);
			}catch(Exception e) {
				e.printStackTrace();
			}
			t2.interrupt();
		}
	}
}