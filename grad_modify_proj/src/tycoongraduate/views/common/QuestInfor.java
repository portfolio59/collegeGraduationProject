package tycoongraduate.views.common;

import java.awt.Graphics;

import javax.swing.JLabel;

import tycoongraduate.model.vo.game.PlayerVo;

public class QuestInfor extends JLabel{
	private PlayerVo player;
	public QuestInfor(PlayerVo player) {
		this.player = player;
		this.setLocation(145,100); //x좌표 변경 : 1000 -> 145
		this.setSize(700,700);
	}
	public void paintComponent(Graphics g) {
		String str = "닉네임 : " + player.getUserId() +", "+ "춤 : "+player.getDanceScore()
		+", "+ "노래 : "+player.getSingScore() 
		+"," +" " + " 총점 : " + player.getTotalScore() ;
		g.drawString(str , 0 ,30);
		super.paintComponent(g);
	}	
}