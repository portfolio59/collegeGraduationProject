package tycoongraduate.views.mainMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.views.mainMap.MainMapFirst;
import tycoongraduate.views.mainMap.MyKeyListenerMainMap;
import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.PlayerLabel;
import tycoongraduate.views.common.QuestInfor;

public class MainMapFirst extends JPanel {	
	private MainFrame mf;
	private MainMapFirst panel = this;
	private PlayerVo player;
	private JLabel quest = new JLabel();
	private QuestInfor questInfor;

	private PlayerLabel playerLabel;
	private JLabel quest2;
	
	public void paintComponent(Graphics g) {
		setFocusable(true);
		requestFocus();
		MapImage map1 = new MapImage();
		//작은 퀘스트화면1 = images/Que1.png
		g.drawImage(map1.que1().getImage(),1000,250,300,200,null);
		//메인도로 화면 = images/mainMap/MainMap.png
		g.drawImage(map1.mainMap().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public MainMapFirst (MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		playerLabel = new PlayerLabel(player);
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		//공원을 찾아가라 말풍선 = images/mainMap/parkQ.png
		DialogImage dialog = new DialogImage();
		Image image = dialog.goToPark().getImage();
		quest.setIcon(new ImageIcon(image));
		quest.setBounds(200,50,1000,700);
		quest.setVisible(true);
		panel.add(quest);
		//퀘스트 1을 하는 중이므로 1로 표시
		player.setQuest(1);
		playerLabel.setLayout(null);
		playerLabel.setBounds(1075, 500, 100, 100);
		this.add(playerLabel);
		this.addKeyListener(new MyKeyListenerMainMap(playerLabel,mf,panel,player));

		JLabel questWindow = new JLabel() {
		public void paintComponent(Graphics g) {
			//작은 퀘스트화면 = images/Que1.png
			MapImage map2 = new MapImage();
			g.drawImage(map2.que1().getImage(),0,0,260,100,null);
			setOpaque(false); 
			super.paintComponent(g);
		}};
		
		questWindow.setLayout(null);
		questWindow.setBounds(1100,10,260,100);
		this.add(questWindow);
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				quest.setVisible(false);
			}
		});
	}
}