package tycoongraduate.views.mainMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.PlayerLabel;
import tycoongraduate.views.common.QuestInfor;

public class MainMapJunguk extends JPanel {	
	private MainFrame mf;
	private MainMapJunguk panel = this;
	private PlayerVo player;
	private QuestInfor questInfor;
	private JLabel questWindow;
	private JLabel questWindow2;
	private MapImage map = new MapImage();
	private PlayerLabel playerLabel;
	
	public void paintComponent(Graphics g) {
		setFocusable(true);
		requestFocus();
		g.drawImage(map.mainMap().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public MainMapJunguk (MainFrame mf, PlayerVo player) {
		this.player = player;
		this.mf = mf;
		playerLabel = new PlayerLabel(player);
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		questWindow = new JLabel() {
			public void paintComponent(Graphics g) {
				//que3  = src/tycoongraduate/images/Que3.png
				g.drawImage(map.que3().getImage(),0,0,260,100,null);
				setOpaque(false); 
				super.paintComponent(g);
			}};
		questWindow.setLayout(null);
		questWindow.setBounds(1100,10,260,100);
		questWindow.setVisible(true);
		this.add(questWindow);
		questWindow2 = new JLabel() {
			public void paintComponent(Graphics g) {
				//que4 = src/tycoongraduate/images/Que4.png
				g.drawImage(map.que4().getImage(),0,0,260,100,null);
				setOpaque(false); 
				super.paintComponent(g);
			}};
		questWindow2.setLayout(null);
		questWindow2.setBounds(1100,10,260,100);
		questWindow2.setVisible(false);
		this.add(questWindow2);
        System.out.println("메인맵 전국 클래스 player.getSingScore() : " + player.getSingScore());
        System.out.println("메인맵 전국 클래스 player.getDanceScore() : " + player.getDanceScore());
		//전국노래 자랑 퀘스트 했으면 다음퀘스트가 보여지도록 설정
		if(player.getQuest() >= 4) {
			questWindow.setVisible(false);
			questWindow2.setVisible(true);
		} else {
			questWindow.setVisible(true);
			questWindow2.setVisible(false);
		}
		playerLabel.setLayout(null);
		playerLabel.setBounds(600, 430, 100, 100);
		this.add(playerLabel);
		this.setFocusable(true);
		addKeyListener(new MyKeyListenerJunGuk(playerLabel, mf, panel, player));
	}
}