package tycoongraduate.views.mainMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.PlayerLabel;
import tycoongraduate.views.common.QuestInfor;

public class MainMapDispatch extends JPanel {	
	private MainFrame mf;
	private MainMapDispatch panel = this;
	private PlayerVo player;
	private QuestInfor questInfor;
	private MapImage map = new MapImage();
	private PlayerLabel playerLabel;

	public void paintComponent(Graphics g) {
		setFocusable(true);
		requestFocus();
		//메인도로 화면 = images/mainMap/MainMap.png
		g.drawImage(map.mainMap().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public MainMapDispatch (MainFrame mf, PlayerVo player) {
		this.player = player;
		this.mf = mf;
		System.out.println("MainMapdispatch 클래스 player.danceScore, singScore : "+ player.getDanceScore() +", " + player.getSingScore());
		playerLabel = new PlayerLabel(player);
		this.addKeyListener(new MyKeyListenerDispatch(playerLabel, mf, panel, player));
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(600,0,500,200);
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
	
		playerLabel.setLayout(null);
		playerLabel.setBounds(600, 430, 100, 100);
		this.add(playerLabel);
		this.setFocusable(true);
	}
}