package tycoongraduate.views.mainMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.Park1;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.PlayerLabel;
import tycoongraduate.views.common.QuestInfor;

public class MainMapSosok extends JPanel {	
	private MainFrame mf;
	private MainMapSosok panel = this;
	private PlayerVo player;
	private QuestInfor questInfor;
	private MapImage map = new MapImage();
	
	private PlayerLabel playerLabel;
	public void paintComponent(Graphics g) {
		setFocusable(true);
		requestFocus();
		//메인 도로 이미지 = images/mainMap/MainMap.png
		g.drawImage(map.mainMap().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public MainMapSosok(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		playerLabel = new PlayerLabel(player);
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		//퀘스트 2를 진행중이므로 +1 추가 , player.getQuest() = 2가 되야함
		player.setQuest(player.getQuest() + 1);
		System.out.println("MainMapSosok 클래스에서 player.getQuest() : " + player.getQuest());
		JLabel questWindow = new JLabel() {
			public void paintComponent(Graphics g) {
				//퀘스트 안내창 = images/Que2.png
				g.drawImage(map.que2().getImage(),0,0,260,100,null);
				setOpaque(false); 
				super.paintComponent(g);
			}};
			questWindow.setLayout(null);
			questWindow.setBounds(1100,10,260,100);
			this.add(questWindow);
		addKeyListener(new MyKeyListenerSosok(playerLabel, mf, panel, player));
		playerLabel.setLayout(null);
		playerLabel.setBounds(630, 130, 100, 100);
		this.add(playerLabel);
		this.setFocusable(true);
	}
}