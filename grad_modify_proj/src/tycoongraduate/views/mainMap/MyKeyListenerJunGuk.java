package tycoongraduate.views.mainMap;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.jungukGame.JungukFirstPanel;
import tycoongraduate.views.sosokMap.Sosok_junguk;
import tycoongraduate.views.trotGame.TrotGameFirstPanel;

public class MyKeyListenerJunGuk extends KeyAdapter{
	private MainFrame mf;
	private JPanel panel;
	private JLabel la;
	private PlayerVo player;
	private static final int MOVE_UNIT = 10;

	public MyKeyListenerJunGuk(JLabel la, MainFrame mf, JPanel panel, PlayerVo player) {
		this.la = la;
		this.mf = mf;
		this.panel = panel;
		this.player = player;
	}
	@Override
	public void keyReleased(KeyEvent e) {
		int a = la.getX();
		int b = la.getY();
		if(player.getDanceScore() >= 2 && player.getSingScore() >= 2) {
			if((a >= 800 && a <= 900)&&(b >= -50 && b <= 0)&& player.getQuest() <= 3) {
				player.setQuest(player.getQuest() + 1);
				//player 진행상태 3이 되야함
				System.out.println("MykeyListenerJunGuk 에서의 player quest 추가 : " + player.getQuest());
				ChangePanel.changePanel(mf, panel, new JungukFirstPanel(mf,player));
				mf.revalidate();
			}
		}
		if(player.getDanceScore() >= 2 && player.getSingScore() >= 2) {
			if((a >= 250 && a <= 350) && (b >= 500 && b <= 580)&& player.getQuest() >= 4) {
				ChangePanel.changePanel(mf, panel, new TrotGameFirstPanel(mf,player));
				mf.revalidate();
			}
		}
		if(player.getDanceScore() < 2 && player.getSingScore() < 2) {
			if((a >= 550 && a <= 700)&&(b >= 400 && b <= 460)) {
				ChangePanel.changePanel(mf, panel, new Sosok_junguk(mf, player));
				mf.revalidate();
			}
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		int a = la.getX();
		int b = la.getY();
		
		switch(keyCode) {
		case KeyEvent.VK_UP:
			if(b >= 0) {
				la.setLocation(la.getX(), la.getY() - MOVE_UNIT);
			}
			break;
		case KeyEvent.VK_DOWN:
			if(b <= 770) {
				la.setLocation(la.getX(), la.getY() + MOVE_UNIT);
			}
			break;
		case KeyEvent.VK_LEFT:
			if(a >= 10) {
				la.setLocation(la.getX() - MOVE_UNIT, la.getY());
			}
			break;
		case KeyEvent.VK_RIGHT :
			if(a <= 1300) {
				la.setLocation(la.getX() + MOVE_UNIT, la.getY());
			}
			break;
		case KeyEvent.VK_SPACE:
			System.out.println("x좌표 : " +la.getX());
			System.out.println("y좌표 : " +la.getY());
		}
	}
}