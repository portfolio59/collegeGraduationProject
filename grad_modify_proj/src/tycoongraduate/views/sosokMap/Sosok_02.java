package tycoongraduate.views.sosokMap;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class Sosok_02 extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private MapImage map = new MapImage();
	private DialogImage dialog = new DialogImage();
	private JLabel door;
	private PlayerVo player;
	
	public Sosok_02(MainFrame mf, PlayerVo player) {
		this.mf=mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		//바깥 문 = images/companyMap/outdoor.png
	    door = new JLabel(bg.outDoor());
	    door.setLayout(null);
	    door.setBounds(0, 300, 40, 250);
	    door.setOpaque(false);
	    this.add(door);
		this.addMouseListener(new clickButton() );
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//소속사 배경 이미지 = images/companyMap/comp_background.png
		g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); 
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("저... 트로트 스타가 되고 싶습니다..", 100, 730);
		//소속사 사장 정적 = images/chracters/boss.png
		g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
		if(player.getGender() == "M") {
			//츄리닝 남자 정적 = images/chracters/CharacterM.png
			g.drawImage(character.characterMpngbf().getImage(), 10, 300, 250, 250, this);
	    } else {
	    	//츄리닝 여자 정적 = images/chracters/CharacterW.png
			g.drawImage(character.characterWpngbf().getImage(), 10, 300, 250, 250, this);
	    }
	}
	class clickButton extends MouseAdapter{  
		@Override
		public void mouseClicked(MouseEvent e) {
			ChangePanel.changePanel(mf, panel, new Sosok_03(mf,player));
			mf.revalidate();
		}
	}
}