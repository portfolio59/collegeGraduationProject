package tycoongraduate.views.sosokMap;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.danceGame.MiniGameDance1;

public class ClickButtonR_Listener extends MouseAdapter{
	private MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	public ClickButtonR_Listener(MainFrame mf, JPanel panel, PlayerVo player) {
		this.mf = mf;
		this.panel = panel;
		this.player = player;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		ChangePanel.changePanel(mf, panel, new MiniGameDance1(mf,player));
		mf.revalidate();
	}
}