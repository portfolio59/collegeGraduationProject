package tycoongraduate.views.sosokMap;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class Sosok_02_trot extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private DialogImage dialog = new DialogImage();
	private MapImage map = new MapImage();
	private BackgroundImage bg = new BackgroundImage();
	private PlayerVo player;

	public Sosok_02_trot(MainFrame mf, PlayerVo player) {
		this.mf=mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new Sosok_03_trot(mf,player));
				mf.revalidate();
			}
		} );
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//회사 소속사 복도 이미지 = images/companyMap/comp_background.png
		g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); 
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("[사장님!! 저 트롯스타 TV 프로그램 오디션 합격했어요!!!]", 100, 730);
		//소속사 사장 정적 = images/chracters/boss.png
		g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
		if(player.getGender() == "M") {
			//변신후 남자 정적 = images/chracters/FCharacterM.png
			g.drawImage(character.characterMpngaf().getImage(), 10, 300, 250, 250, this);
		} else {
			//변신후 여자 정적 = images/chracters/FCharacterW.png
			g.drawImage(character.characterWpngaf().getImage(), 10, 300, 250, 250, this);
		}
	}
}
