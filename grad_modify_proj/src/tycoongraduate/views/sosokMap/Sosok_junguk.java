package tycoongraduate.views.sosokMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.QuestInfor;
import tycoongraduate.views.mainMap.MainMapJunguk;

public class Sosok_junguk extends JPanel { 
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private MapImage map = new MapImage();
	private JLabel doorMain;
	private PlayerVo player;
	private QuestInfor questInfor;
	
	public Sosok_junguk(MainFrame mf, PlayerVo player) {
		this.mf=mf;
		panel = this;
		this.player = player;
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(600,0,500,200);
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		this.setLayout(null);
		//나가는 문 이미지 = images/companyMap/outdoor.png
		doorMain = new JLabel(bg.outDoor());
		doorMain.setLayout(null);
		doorMain.setBounds(0, 300, 40, 250);
		doorMain.setOpaque(false);
		this.add(doorMain);
		//춤 능력, 노래 능력 점수가 모두 최소 2점이 넘어야 나갈수있음
		if(player.getSingScore() >= 2 && player.getDanceScore() >= 2) {
			doorMain.addMouseListener(new MouseAdapter() {
	            @Override
	            public void mouseClicked(MouseEvent e) {
	               ChangePanel.changePanel(mf, panel, new MainMapJunguk(mf,player));
	            }
	        });
	    }
		//큰문(오른쪽)이미지 부착 = images/companyMap/door.png
		JLabel labelR = new JLabel(bg.bigDoor());
		JButton doorR = new JButton();
		doorR.setBounds(485,127,193,276);
		doorR.setBorderPainted(false);
		doorR.setContentAreaFilled(false);
		doorR.setFocusPainted(false);
		doorR.setBorderPainted(false);
		doorR.add(labelR);
		panel.add(doorR);
		doorR.addMouseListener(new ClickButtonR_Listener(mf, panel, player));
		
		//큰문(왼쪽)이미지 부착 = images/companyMap/door.png
        JLabel labelL = new JLabel(bg.bigDoor());
        JButton doorL = new JButton();
        doorL.setBounds(135,127,193,276);
        doorL.setBorderPainted(false);
        doorL.add(labelL);
        doorL.setBorderPainted(false);
        doorL.setContentAreaFilled(false);
        doorL.setFocusPainted(false);
        panel.add(doorL);
        doorL.addMouseListener(new ClickButtonL_Listener(mf, panel, player));
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//소속사 복도 배경 이미지 = images/companyMap/comp_background.png
		g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); // 0,600,1400,250
		//소속사 사장 정적 = images/chracters/boss.png
		g.drawImage(character.boss().getImage(), 920, 300, 250, 250, this);
		if(player.getGender() == "M") {
			//츄리닝 남자 정적 = images/chracters/CharacterM.png
			g.drawImage(character.characterMpngbf().getImage(), 750, 300, 250, 250, this);
		} else {
			//츄리닝 여자 정적 = images/chracters/CharacterW.png
			g.drawImage(character.characterWpngbf().getImage(), 750, 300, 250, 250, this);
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		if(player.getSingScore() >= 2 && player.getDanceScore() >= 2 && player.getQuest() <= 3) {
			g.drawString("사장님: 이제야...! 이제 트로트 스타의 자격이 충분한 거 같구나... 이정도로 성장할 줄이야..", 100, 710);
			g.drawString("소속사를 나가서 전국 노래자랑에 참가를 해보거라!", 140, 750);
		} else if(player.getSingScore() >= 2 && player.getDanceScore() >= 2 && player.getQuest() >= 4) {
			g.drawString("사장님: 트롯스타 테스트를 위해서 춤 능력과 노래 능력을 올리거라! 그리고,", 100, 710);
			g.drawString("준비가 되면 방송국으로 찾아가서 트롯스타 테스트를 보고 오너라!", 140, 750);
		} else {
			g.drawString("사장님: 아직은...트로트 스타로 가려면 부족해... ", 100, 710);
			g.drawString("열심히 노력해서 춤과 노래 능력을 더 쌓으거라!", 140, 750);
		}
	}
}