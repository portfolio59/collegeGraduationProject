package tycoongraduate.views.sosokMap;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.suddenGame.SuddenGameStart;

public class Sosok_01_trot extends JPanel { 
   private MainFrame mf;
   private JPanel panel;
   private CharacterImage character = new CharacterImage();
   private DialogImage dialog = new DialogImage();
   private MapImage map = new MapImage();
   private BackgroundImage bg = new BackgroundImage();
   private JLabel door;
   
   private PlayerVo player;
   
   public Sosok_01_trot(MainFrame mf, PlayerVo player) {
      this.mf=mf;
      panel = this;
      this.player = player;
      this.setLayout(null);
      //바깥 문 = images/companyMap/outdoor.png
      door = new JLabel(bg.outDoor());
      door.setLayout(null);
      door.setBounds(0, 300, 40, 250);
      door.setOpaque(false);
      this.add(door);
      //ousdoor클릭하면 돌발이벤트게임으로 가는지 확인
      door.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            ChangePanel.changePanel(mf, panel, new SuddenGameStart(mf,player));
         }
      });
      this.addMouseListener(new MouseAdapter() {
    	  @Override
    	  public void mouseClicked(MouseEvent e) {
    		  ChangePanel.changePanel(mf, panel, new Sosok_02_trot(mf,player));
    		  mf.revalidate();
    	  }
      } );
   }
   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      //회사 소속사 복도 이미지 = images/companyMap/comp_background.png
      g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
      //말풍선 틀 이미지 = images/dialog_format.png
      g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null);
      //소속사 사장 정적 = images/chracters/boss.png
      g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
      if(player.getGender() == "M") {
    	  //변신후 남자 정적 = images/chracters/FCharacterM.png
    	  g.drawImage(character.characterMpngaf().getImage(), 10, 300, 250, 250, this);
      }else {
    	  //변신후 여자 정적 = images/chracters/FCharacterW.png
    	  g.drawImage(character.characterWpngaf().getImage(), 10, 300, 250, 250, this);
      }
      g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
      g.drawString("[사장님...]", 100, 730);
   }
}