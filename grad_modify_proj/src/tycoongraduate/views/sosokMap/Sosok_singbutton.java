package tycoongraduate.views.sosokMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.QuestInfor;
import tycoongraduate.views.mainMap.MainMapJunguk;

public class Sosok_singbutton extends JPanel{
      private MainFrame mf;
      private Sosok_singbutton panel = this;
      private JLabel door;
      private CharacterImage character = new CharacterImage();
      private MapImage map = new MapImage();
      private BackgroundImage bg = new BackgroundImage();
      private DialogImage dialog = new DialogImage();
      private PlayerVo player;
      private QuestInfor questInfor;
      private Image cimage;
      
      public Sosok_singbutton(MainFrame mf, PlayerVo player) {
         this.mf = mf;
         this.player = player;
         this.setLayout(null);
         questInfor = new QuestInfor(player);
 		 questInfor.setLayout(null);
 		 questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
 		 questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
 		 questInfor.setForeground(Color.WHITE);
 		 panel.add(questInfor);
 		 
 		 if(player.getGender() == "M" && player.getQuest() != 4) {
 			 //츄리닝 남자 정적 = images/chracters/CharacterM.png
 			 cimage = character.characterMpngbf().getImage().getScaledInstance(250, 250, 0);
 		 } else if(player.getGender() == "W" && player.getQuest() != 4){
 			 //츄리닝 여자 정적 = images/chracters/CharacterW.png
 			 cimage = character.characterWpngbf().getImage().getScaledInstance(250, 250, 0);
 		 } else if(player.getGender() == "M" && player.getQuest() == 4) {
 			cimage = character.characterMpngaf().getImage().getScaledInstance(250, 250, 0);
 		 } else if(player.getGender() == "W" && player.getQuest() == 4) {
 			cimage = character.characterWpngaf().getImage().getScaledInstance(250, 250, 0);
 		 }
         //소속사 사장 정적 = images/chracters/boss.png
         Image simage  = character.boss().getImage().getScaledInstance(250, 250, 0);
         ImageIcon cIcon = new ImageIcon(cimage);
         ImageIcon sIcon = new ImageIcon(simage);
         //바깥 문 = images/companyMap/outdoor.png
         door = new JLabel(bg.outDoor());
         door.setLayout(null);
         door.setBounds(0, 300, 40, 250);
         door.setOpaque(false);
         this.add(door);
         System.out.println("소속 싱버튼 클래스 player.getSingScore() : " + player.getSingScore());
         System.out.println("소속 싱버튼 클래스 player.getDanceScore() : " + player.getDanceScore());
         if(player.getSingScore() >= 2 && player.getDanceScore() >= 2 || player.getQuest() >= 4) {
         door.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               ChangePanel.changePanel(mf, panel, new MainMapJunguk(mf,player));
            }
         });
         }
         JLabel cLabel = new JLabel(cIcon);
         JLabel sLabel = new JLabel(sIcon);
         cLabel.setBounds(30, 250, 398, 398);

         JButton sajangButton = new JButton();
         sajangButton.add(sLabel);
         sajangButton.setBorderPainted(false);
         sajangButton.setContentAreaFilled(false);
         
         sajangButton.setBounds(300, 300, 250, 250);
         panel.add(cLabel);
         panel.add(sajangButton);
         //큰문(왼쪽)이미지 부착 = images/companyMap/door.png
         JLabel labelL = new JLabel(bg.bigDoor());
         JButton doorL = new JButton();
         doorL.setBounds(135,127,193,276);
         doorL.setBorderPainted(false);
         doorL.add(labelL);
         doorL.setBorderPainted(false);
         doorL.setContentAreaFilled(false);
         doorL.setFocusPainted(false);
         panel.add(doorL);
         doorL.addMouseListener(new ClickButtonL_Listener(mf, panel, player));
      }
      public void paintComponent(Graphics g) {
    	 //소속사 배경 이미지 = images/companyMap/comp_background.png
         g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(), null);
         //말풍선 틀 이미지 = images/dialog_format.png
         g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250, null);
         g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
         if(player.getSingScore() >= 2 && player.getDanceScore() >= 2) {
           	 g.drawString("밖으로 나가서 방송국을 찾아가 보자!", 100, 730);
         }else {
        	 g.drawString("그럼 노래능력부터 쌓으러 가볼까?",100,730);       
         }
         System.out.println("sosok_singbutton 클래스 : player.getQuest() 4예상 : " + player.getQuest());
         setOpaque(false);
         super.paintComponent(g);
      }
   }