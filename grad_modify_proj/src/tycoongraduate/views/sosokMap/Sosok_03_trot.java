package tycoongraduate.views.sosokMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.QuestInfor;
import tycoongraduate.views.suddenGame.SuddenGameStart;

public class Sosok_03_trot extends JPanel { 
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private DialogImage dialog = new DialogImage();
	private MapImage map = new MapImage();
	private BackgroundImage bg = new BackgroundImage();
	private JLabel door;
	private PlayerVo player;
	private QuestInfor questInfor;

	public Sosok_03_trot(MainFrame mf, PlayerVo player) {
		this.mf=mf;
		panel = this;
		this.player = player;
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(600,0,500,200);
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		this.setLayout(null);
		//바깥 문 = images/companyMap/outdoor.png
		door = new JLabel(bg.outDoor());
		door.setLayout(null);
		door.setBounds(0, 300, 40, 250);
		door.setOpaque(false);
		this.add(door);
		door.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new SuddenGameStart(mf,player));
			}
		});
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//회사 소속사 복도 이미지 = images/companyMap/comp_background.png
		g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); 
		//소속사 사장 느낌표 이미지 정적 = images/chracters/boss2.png
		g.drawImage(character.bossMark().getImage(), 300, 300, 250, 250, this);
		if(player.getGender() == "M") {
			//변신후 남자 정적 = images/chracters/FCharacterM.png
			g.drawImage(character.characterMpngaf().getImage(), 10, 300, 250, 250, this);
		}else {
			//변신후 여자 정적 = images/chracters/FCharacterW.png
			g.drawImage(character.characterWpngaf().getImage(), 10, 300, 250, 250, this);
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("사장님: 나는 역시 네가 해낼 줄 알았다! ", 100, 710);
		g.drawString("이제 오디션에 합격했으니 앞으로 데뷔하는 건 문제가 없겠구나", 100, 750);
		g.drawString("네가 전국 노래자랑에서 인기를 끈 덕분에 밖에 기자들이 난리구나. 한번 나가보거라!", 100, 790);
	}
}