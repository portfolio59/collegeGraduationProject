package tycoongraduate.views.sosokMap;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class Sosok_03 extends JPanel { 
   private MainFrame mf;
   private JPanel panel;
   private JLabel door;
   private PlayerVo player;
   private DialogImage dialog = new DialogImage();
   private BackgroundImage bg = new BackgroundImage();
   private MapImage map = new MapImage();
   private CharacterImage character = new CharacterImage();

   public Sosok_03(MainFrame mf, PlayerVo player) {
	  this.mf=mf;
	  panel = this;
	  this.player = player;
	  this.setLayout(null);
	  //바깥 문 = images/companyMap/outdoor.png
	  door = new JLabel(bg.outDoor());
	  door.setLayout(null);
	  door.setBounds(0, 300, 40, 250);
	  door.setOpaque(false);
	  this.add(door);
	  this.addMouseListener(new clickButton() );
   }

   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      //소속사 배경 이미지 = images/companyMap/comp_background.png
      g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(), null);
      //말풍선 틀 이미지 = images/dialog_format.png
      g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); 
      //소속사 사장 정적 = images/chracters/boss.png
      g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
      if(player.getGender() == "M") {
		  //츄리닝 남자 정적 = images/chracters/CharacterM.png
		  g.drawImage(character.characterMpngbf().getImage(), 10, 300, 250, 250, this);
      } else {
    	  //츄리닝 여자 정적 = images/chracters/CharacterW.png
		  g.drawImage(character.characterWpngbf().getImage(), 10, 300, 250, 250, this);
      }
      g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
      g.drawString("사장님 : 지금부터 내가 트로트 스타가 되기 위한 미션을 내줄테니..", 100, 700);
      g.drawString("춤 능력 2점, 노래 능력 2점을 쌓아서 나에게 오게나.", 100, 750);
      g.drawString("각 방에 들어가면 능력을 쌓을 수 있을게야", 100, 800);
   } 
   class clickButton extends MouseAdapter{  
      @Override
      public void mouseClicked(MouseEvent e) {
         ChangePanel.changePanel(mf, panel, new Sosok_singbutton(mf,player));
         mf.revalidate();
      }
   }
}