package tycoongraduate.views.sosokMap;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class Sosok_03_second extends JPanel { 
   private MainFrame mf;
   private JPanel panel;
   private MapImage map = new MapImage();
   private CharacterImage character = new CharacterImage();
   private DialogImage dialog = new DialogImage();
   private PlayerVo player;

   public Sosok_03_second(MainFrame mf, PlayerVo player) {
      this.mf=mf;
      panel = this;
      this.player = player;
      this.setLayout(null);
      this.addMouseListener(new MouseAdapter() {
    	  @Override
          public void mouseClicked(MouseEvent e) {
             ChangePanel.changePanel(mf, panel, new Sosok_singbutton(mf,player));
             mf.revalidate();
          }
	} );
   }
   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      //회사 소속사 복도 이미지 = images/companyMap/comp_background.png
      g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
      //말풍선 틀 이미지 = images/dialog_format.png
      g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null);
      //소속사 사장 정적 = images/chracters/boss.png
      g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
      if(player.getGender() == "M") {
    	  //변신후 남자 정적 = images/chracters/FCharacterM.png
    	  g.drawImage(character.characterMpngaf().getImage(), 10, 300, 250, 250, this);
      } else {
    	  //변신후 여자 정적 = images/chracters/FCharacterW.png
    	  g.drawImage(character.characterWpngaf().getImage(), 10, 300, 250, 250, this);
      }
      g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
      g.drawString("사장님 : 아니, 결국 해냈구나!!! 잘했다! 역시 내 눈은 틀리지 않았어", 100, 700);
      g.drawString("며칠 후에 트롯스타 TV 프로그램 오디션을 본다던데 한번 참가해보자 꾸나!!", 120, 750);
   }
}