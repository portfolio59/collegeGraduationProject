package tycoongraduate.views.sosokMap;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.QuestInfor;
import tycoongraduate.views.mainMap.MainMapJunguk;

public class Sosok_01 extends JPanel { 
   private MainFrame mf;
   private JPanel panel;
   private CharacterImage character = new CharacterImage();
   private MapImage map = new MapImage();
   private DialogImage dialog = new DialogImage();
   private BackgroundImage bg = new BackgroundImage();
   private JLabel door;
   private PlayerVo player;
   private QuestInfor questInfor;
   
   public Sosok_01(MainFrame mf, PlayerVo player) {
      this.mf=mf;
      panel = this;
      this.player  = player;
      this.setLayout(null);
      questInfor = new QuestInfor(player);
      questInfor.setLayout(null);
      questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
      questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
      questInfor.setForeground(Color.WHITE);
      panel.add(questInfor);
      //퀘스트2를 잘 지나왔음으로 다음 퀘스트 진행상태 변경
      player.setQuest(2);
      //바깥 문 = images/companyMap/outdoor.png
      door = new JLabel(bg.outDoor());
      door.setLayout(null);
      door.setBounds(0, 300, 40, 250);
      door.setOpaque(false);
      this.add(door);

      door.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            ChangePanel.changePanel(mf, panel, new MainMapJunguk(mf,player));
         }
      });
      this.addMouseListener(new clickButton() );
   }
   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      //소속사 배경 이미지 = images/companyMap/comp_background.png
      g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
      //말풍선 틀 이미지 = images/dialog_format.png
      g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); 
      //소속사 사장 정적 = images/chracters/boss.png
      g.drawImage(character.boss().getImage(), 300, 300, 250, 250, this);
      if(player.getGender() == "M") {
		  //츄리닝 남자 정적 = images/chracters/CharacterM.png
		  g.drawImage(character.characterMpngbf().getImage(), 10, 300, 250, 250, this);
      } else {
    	//츄리닝 여자 정적 = images/chracters/CharacterW.png
		  g.drawImage(character.characterWpngbf().getImage(), 10, 300, 250, 250, this);
      }
      g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
      g.drawString("[JYB 사장님이 다가온다...]", 100, 730);
   }
   class clickButton extends MouseAdapter{  
      @Override
      public void mouseClicked(MouseEvent e) {
         ChangePanel.changePanel(mf, panel, new Sosok_02(mf,player));
         mf.revalidate();
      }
   }
}