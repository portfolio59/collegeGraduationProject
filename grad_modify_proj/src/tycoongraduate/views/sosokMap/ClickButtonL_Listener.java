package tycoongraduate.views.sosokMap;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.singGame.MiniGameSsing1;

public class ClickButtonL_Listener extends MouseAdapter{
	private MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	
	public ClickButtonL_Listener(MainFrame mf, JPanel panel, PlayerVo player) {
		this.mf = mf;
		this.panel = panel;
		this.player = player;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		ChangePanel.changePanel(mf, panel, new MiniGameSsing1(mf,player));
		mf.revalidate();
	}
}