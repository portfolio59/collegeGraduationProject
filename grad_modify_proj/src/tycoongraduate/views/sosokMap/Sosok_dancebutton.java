package tycoongraduate.views.sosokMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.QuestInfor;
import tycoongraduate.views.mainMap.MainMapJunguk;

public class Sosok_dancebutton extends JPanel{
	private MainFrame mf;
	private Sosok_dancebutton panel = this;
	private JLabel doorMain;
	private MapImage map = new MapImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private CharacterImage character = new CharacterImage();
	private PlayerVo player;
	private QuestInfor questInfor;
	private Image cimage;
	
	public Sosok_dancebutton(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		questInfor = new QuestInfor(player);
		questInfor.setLayout(null);
		questInfor.setBounds(400,0,900,200); //600, 0, 500, 200 -> 400, 0, 900, 200
		questInfor.setFont(new Font("Neo둥근모", Font.BOLD, 25));
		questInfor.setForeground(Color.WHITE);
		panel.add(questInfor);
		this.setLayout(null);
		
		if(player.getGender() == "M") {
			 //츄리닝 남자 정적 = images/chracters/CharacterM.png
			 cimage = character.characterMpngbf().getImage().getScaledInstance(250, 250, 0);
		 } else {
			 //츄리닝 여자 정적 = images/chracters/CharacterW.png
			 cimage = character.characterWpngbf().getImage().getScaledInstance(250, 250, 0);
		 }
		//소속사 사장 정적 = images/chracters/boss.png
		Image simage  = character.boss().getImage().getScaledInstance(250, 250, 0);
		ImageIcon cIcon = new ImageIcon(cimage);
		ImageIcon sIcon = new ImageIcon(simage);
		//나가는 문 이미지 = images/companyMap/outdoor.png
		doorMain = new JLabel(bg.outDoor());
		doorMain.setLayout(null);
		doorMain.setBounds(0, 300, 40, 250);
		doorMain.setOpaque(false);
		this.add(doorMain);
		if(player.getSingScore() >= 2 && player.getDanceScore() >= 2) {
	         doorMain.addMouseListener(new MouseAdapter() {
	            @Override
	            public void mouseClicked(MouseEvent e) {
	               ChangePanel.changePanel(mf, panel, new MainMapJunguk(mf,player));
	            }
	         });
	    }
		JLabel cLabel = new JLabel(cIcon);
		JLabel sLabel = new JLabel(sIcon);
		cLabel.setBounds(30, 250, 398, 398);

		JButton sajangButton = new JButton();
		sajangButton.add(sLabel);
		sajangButton.setBorderPainted(false);
		sajangButton.setContentAreaFilled(false);

		sajangButton.setBounds(300, 250, 398, 398);
		panel.add(cLabel);
		panel.add(sajangButton);
		
		//큰문(오른쪽)이미지 부착 = images/companyMap/door.png
		JLabel labelR = new JLabel(bg.bigDoor());
		JButton doorR = new JButton();
		doorR.setBounds(485,127,193,276);
		doorR.setBorderPainted(false);
		doorR.setContentAreaFilled(false);
		doorR.setFocusPainted(false);
		doorR.setBorderPainted(false);
		doorR.add(labelR);
		panel.add(doorR);
		doorR.addMouseListener(new ClickButtonR_Listener(mf, panel, player));
	}
	public void paintComponent(Graphics g) {
		//소속사 복도 배경 이미지 = images/companyMap/comp_background.png
		g.drawImage(map.compMap().getImage(), 0, 0, this.getWidth(),this.getHeight(), null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250, null);
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("이제 춤 능력을 올리러 가볼까!",100, 700);
		setOpaque(false);
		super.paintComponent(g);
	}
}