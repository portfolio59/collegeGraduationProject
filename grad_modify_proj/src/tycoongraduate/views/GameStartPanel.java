package tycoongraduate.views;

import java.awt.Graphics;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.views.common.ChangePanel;

public class GameStartPanel extends JPanel {
	private MainFrame mf;
	private GameStartPanel panel = this;

	public void paintComponent(Graphics g) {	
		BackgroundImage bg = new BackgroundImage();
		g.drawImage(bg.firstScreen().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mf.repaint();
	}
	public GameStartPanel(MainFrame mf) {
		this.mf = mf;
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new NickName(mf));	
			}
		});
	}
}	