package tycoongraduate.views.suddenGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.game.SuddenGameVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.ending.EndingBackSudden;
import tycoongraduate.views.mainMap.MainMapDispatch;

public class SuddenGamePanel2 extends JPanel {
	//x1, y1 는 현재 클릭한 버튼의 x, y 좌표
	private int x1 = 0;
	private int y1 = 0;
	//x2, y2는 다음에 클릭한 버튼의 x, y 좌표
	private int x2 = 0;
	private int y2 = 0;
	//클릭 1번인지 0번인지 체크 변수
	private int clicks = 0;
	//인덱스처럼 쓰일 예정, ex) 버튼 배열에서 인덱스로 이 버튼이 어디버튼인지 알려줌
	private int indexNum = 0;
	private MainFrame mf;
	private PlayerVo player;
	//클리어하면 얻어지는 점수
	private int score = 150;
	Timer timer;
	//제한 시간 초 변수, 60초 에서 시작
	private int t = 60;
	private Set<Integer> set1 = new LinkedHashSet<Integer>();
	private Set<Integer> set2 =  new LinkedHashSet<Integer>();
	private JPanel panel;
	private JButton [] btnArr = new JButton[9];
	private JLabel [] ans = new JLabel[9];
	//퍼즐 위치 배열
	private int [] locationArr = new int [3];
	//퍼즐 위치값 배열
	private int[] locationValueArr = new int[3];
	//퍼즐 위치값 배열초기화에 쓰이는 증가변수, 초기값 20
	private int num = 20;
	private Image[] puzzleArr = new Image[9];
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private ButtonImage btnImg = new ButtonImage();
	private SuddenGameVo sgv = new SuddenGameVo();
	public void paintComponent(Graphics g) {
		//주인공이 서있는 배경 이미지 = images/miniGame/sudden_game/bground_0.jpg
		g.drawImage(bg.suddenBground().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
		mf.revalidate();
	}
	public SuddenGamePanel2(MainFrame mf,  PlayerVo player) {
		this.setSize(1400,900);
		this.setLayout(null);
		this.mf = mf;
		this.player = player;
		this.panel = this;
		//이미지 불러오기
		for(int i = 1; i <= puzzleArr.length ; i++) {
			puzzleArr[i-1] = sgv.puzzleImg(i).getImage().getScaledInstance(150, 150, 0);
			btnArr[i-1] = new JButton(new ImageIcon(puzzleArr[i-1]));
		}
		//버튼에 퍼즐이미지 붙이기
		for (int i = 0; i < ans.length; i++) {
			ans[i]= new JLabel();
			ans[i].setSize(150, 150);
			ans[i].setName(Integer.valueOf(i).toString()); //ans[0]~ans[8] 이름 = 0~ 8
		}
		for(int i = 0 ; i < btnArr.length ; i++) {
			btnArr[i].setSize(150, 150);
			btnArr[i].setName(Integer.valueOf(i).toString());
			this.add(ans[i]);
		}
		for(int i = 0 ; i < locationValueArr.length ; i++) {
			//num 초기값 20
			locationValueArr[i] = num;
			num += 155;
		}
		for(int i = 0; i < locationValueArr.length ; i++) {
			locationArr[i] = locationValueArr[i];
		}

		//랜덤으로 버튼들의 위치 지정하기 위해 섞는 과정
		while ((set1.size() <4) && (set2.size() <4)) {
			set1.add( locationArr[(int)(Math.random()*3)]);
			set2.add( locationArr[(int)(Math.random()*3)]);
			if ((set1.size() == 3) && (set2.size() == 3)) {
				break;
			}
		}
		Object[] obj1 = set1.toArray(); 
		Object[] obj2 = set2.toArray();
		//버튼배열에 위치지정
		//이중 포문에 이프문 겹쳐 쓰려다가 안되서 포기...
		btnArr[0].setLocation((int)obj1[0],(int)obj2[0]);
		btnArr[1].setLocation((int)obj1[0],(int)obj2[1]);
		btnArr[2].setLocation((int)obj1[0],(int)obj2[2]);
		btnArr[3].setLocation((int)obj1[1],(int)obj2[0]);
		btnArr[4].setLocation((int)obj1[1],(int)obj2[1]);
		btnArr[5].setLocation((int)obj1[1],(int)obj2[2]);
		btnArr[6].setLocation((int)obj1[2],(int)obj2[0]);
		btnArr[7].setLocation((int)obj1[2],(int)obj2[1]);
		btnArr[8].setLocation((int)obj1[2],(int)obj2[2]);
		for(int i = 0 ; i < ans.length ; i++) {
			if(i < 3) {
				for(int j = 0 ; j < 3 ; j++) {
					ans[i].setLocation(locationValueArr[0], locationValueArr[j]);					
				}
			} else if(2 < i && i < 6) {
				for(int j = 0 ; j < 3 ; j++) {
					ans[i].setLocation(locationValueArr[1], locationValueArr[j]);
				}
			} else {
				for(int j = 0 ; j < 3 ; j++) {
					ans[i].setLocation(locationValueArr[2], locationValueArr[j]);
				}
			}
		}
		//답은 x,y좌표로 두개씩묶어서 임
		int answer [] = {locationValueArr[0],locationValueArr[0],locationValueArr[1],locationValueArr[0]
						,locationValueArr[2],locationValueArr[0],locationValueArr[0],locationValueArr[1]
						,locationValueArr[1],locationValueArr[1],locationValueArr[2],locationValueArr[1]
						,locationValueArr[0],locationValueArr[2],locationValueArr[1],locationValueArr[2]
						,locationValueArr[2],locationValueArr[2]};
		//사용자가 입력한 값
		int submit [] = new int[18];
		this.setOpaque(false);
		for(int i = 0 ; i < btnArr.length; i++) {
			this.add(btnArr[i]);
		}
		btnArr[0].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for(int i= 0; i < submit.length ; i++) {
					System.out.println("btnArr[0] ActionListener에서 사용자가 입력한 submit["+i+"]의값: " + submit[i]);
					System.out.println("btnArr[0] ActionListener에서 사용자가 입력한 answer["+i+"]의값: " + answer[i]);
					
				}
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer) == false) {
					if (clicks == 0) {
						//클릭을 안했을 때 버튼의 위치값을 받아옴
						//클릭 후엔 버튼을 안보이게 하고 클릭수를 1증가
						indexNum = 0;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks == 1 && btnArr[indexNum] != btnArr[0]) {
						//지금 클릭한 이 버튼이여야 하고 다른 버튼을 클릭했을 때
						x2 = btnArr[0].getX();
						y2 = btnArr[0].getY();
						//누른 다른 버튼의 위치를 그전에 누른 버튼의 위치와 바꾼다
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[0].setLayout(null);
						btnArr[0].setLocation(x1,y1);
						btnArr[0].setVisible(true);
						panel.repaint();
						clicks -= 1;	
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[1].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 1;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[1]) {
						x2=btnArr[1].getX();
						y2=btnArr[1].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[1].setLayout(null);
						btnArr[1].setLocation(x1,y1);
						btnArr[1].setVisible(true);
						panel.repaint();
						clicks -= 1;	
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[2].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 2;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[2]) {
						x2=btnArr[2].getX();
						y2=btnArr[2].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[2].setLayout(null);
						btnArr[2].setLocation(x1,y1);
						btnArr[2].setVisible(true);
						panel.repaint();
						clicks -= 1;	
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[3].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 3;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[3]) {
						x2=btnArr[3].getX();
						y2=btnArr[3].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[3].setLayout(null);
						btnArr[3].setLocation(x1,y1);
						btnArr[3].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[4].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 4;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[4]) {
						x2=btnArr[4].getX();
						y2=btnArr[4].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[4].setLayout(null);
						btnArr[4].setLocation(x1,y1);
						btnArr[4].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[5].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 5;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[5]) {
						x2=btnArr[5].getX();
						y2=btnArr[5].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[5].setLayout(null);
						btnArr[5].setLocation(x1,y1);
						btnArr[5].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[6].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks ==0) {
						indexNum = 6;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks ==1 && btnArr[indexNum] != btnArr[6]) {
						x2=btnArr[6].getX();
						y2=btnArr[6].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[6].setLayout(null);
						btnArr[6].setLocation(x1,y1);
						btnArr[6].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[7].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)==false) {
					if (clicks == 0) {
						indexNum = 7;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks == 1 && btnArr[indexNum] != btnArr[7]) {
						x2=btnArr[7].getX();
						y2=btnArr[7].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[7].setLayout(null);
						btnArr[7].setLocation(x1,y1);
						btnArr[7].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		btnArr[8].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(submit,answer)== false) {
					if (clicks == 0) {
						indexNum = 8;
						x1 = btnArr[indexNum].getX();
						y1 = btnArr[indexNum].getY();
						btnArr[indexNum].setVisible(false);
						btnArr[indexNum].setLayout(null);
						panel.repaint();
						clicks += 1;	
					}
					else if (clicks == 1 && btnArr[indexNum] != btnArr[8]) {
						x2=btnArr[8].getX();
						y2=btnArr[8].getY();
						btnArr[indexNum].setLocation(x2,y2);
						btnArr[indexNum].setVisible(true);
						btnArr[8].setLayout(null);
						btnArr[8].setLocation(x1,y1);
						btnArr[8].setVisible(true);
						panel.repaint();
						clicks -= 1;
					}
					for ( int i = 0; i < (submit.length/2); i++) {
						submit[i*2] = btnArr[i].getX();
						submit[(i*2)+1] = btnArr[i].getY();
					}
				}
				panel.repaint();
				}		
		});
		JButton fail = new JButton() {
			public void paintComponent(Graphics g) {
				//돌발 게임 실패 이미지 = images/suddenGame/fail.png
				g.drawImage(dialog.suddenDialogFail().getImage(), 0, 0, 1350, 220, null);
				setOpaque(false); 
				super.paintComponent(g);
			}
		}; 
		fail.setLayout(null);
		fail.setBorderPainted(false);
		fail.setContentAreaFilled(false);
		fail.setFocusPainted(false);
		fail.setBounds(20,630,1350,220);
		fail.setVisible(false);
		fail.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new EndingBackSudden(mf,player));
				mf.revalidate();
			}
		});
		panel.add(fail);
		Font neo = new Font("Neo둥근모",Font.BOLD,20);
		JLabel timerLabel = new JLabel("남은 시간 ");
		JButton passBtn = new JButton() {
			public void paintComponent(Graphics g) {
				//돌발 게임 통과 이미지 = images/suddenGame/pass.png
				g.drawImage(dialog.suddenDialogPass().getImage(), 0, 0, 1350, 220, null);
				setOpaque(false); 
				super.paintComponent(g);
			}
		}; 
		passBtn.setLayout(null);
		passBtn.setBorderPainted(false);
		passBtn.setContentAreaFilled(false);
		passBtn.setFocusPainted(false);
		passBtn.setBounds(20,630,1350,220);
		passBtn.setVisible(false);
		passBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new MainMapDispatch(mf,player));
				mf.revalidate();
			}
		});
		this.add(passBtn);
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timerLabel.setText("남은 시간 " + (t-1));
				t--;
				if ((t)==0) {
					timer.stop();
					fail.setVisible(true);
					}
				}
		}	
				);
		timerLabel.setFont(neo);
		timerLabel.setForeground(Color.white);
		timerLabel.setLayout(null);
		timerLabel.setBounds(1000,20,300,200);
		panel.add(timerLabel);
		timer.start();
		JButton submitBtn = new JButton() {
			public void paintComponent(Graphics g) {
				//submit 버튼 이미지 = images/submit.png
				g.drawImage(btnImg.submit_btn().getImage(), 0, 0, 150, 50, null);
				setOpaque(false); 
				super.paintComponent(g);
			}
		};
		submitBtn.setLayout(null);
		submitBtn.setBorderPainted(false);
		submitBtn.setContentAreaFilled(false);
		submitBtn.setFocusPainted(false);
		submitBtn.setBounds(800,50,150,50);
		panel.add(submitBtn);
		//submit 버튼을 눌렀을 때 정답이 맞는지 확인하는 로직
		submitBtn.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				if(SuddenGameCheckAnswer.checkAnswer(answer,submit) == true) {
					//성공
					timer.stop();
					player.setTotalScore(player.getTotalScore() + score);
					passBtn.setVisible(true);	
				}else {
					//실패
					timer.stop();
					fail.setVisible(true);
					fail.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							ChangePanel.changePanel(mf, panel, new EndingBackSudden(mf,player));
							mf.revalidate();
						}
					});
				}
			}
		});
	}
}