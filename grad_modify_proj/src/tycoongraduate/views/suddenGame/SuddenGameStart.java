package tycoongraduate.views.suddenGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class SuddenGameStart extends JPanel {
	private MainFrame mf;
	private PlayerVo player;
	private JPanel panel = this;
	private JLabel suddenGamebanner = new JLabel();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	public void paintComponent(Graphics g) {
		//주인공이 서있는 배경 이미지 = images/miniGame/sudden_game/bground_0.jpg
		g.drawImage(bg.suddenBground().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public SuddenGameStart(MainFrame mf,PlayerVo player) {
		//돌발 게임 배너 이미지 = images/miniGame/sudden_game/banner.jpg
		suddenGamebanner.setIcon(dialog.suddenDialogBanner());
		suddenGamebanner.setLayout(null);
		suddenGamebanner.setBounds(200,50,1000,700);
		this.add(suddenGamebanner);
		suddenGamebanner.setVisible(true);
		//돌발게임으로 향하는 첫 시작 대화창 이미지 = images/miniGame/sudden_game/start.png
		JLabel startDialogLabel = new JLabel(new ImageIcon(dialog.suddenDialogStart().getImage().getScaledInstance(1000, 700, 0)));
		suddenGamebanner.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				suddenGamebanner.setVisible(false);
				startDialogLabel.setVisible(true);
			}
		});
		startDialogLabel.setOpaque(false);
		startDialogLabel.setLayout(null);
		startDialogLabel.setVisible(false);
		startDialogLabel.setBounds(200,50,1000,700);
		this.add(startDialogLabel);
		startDialogLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new SuddenGamePanel1(mf,player));
			}
		});
	}
}