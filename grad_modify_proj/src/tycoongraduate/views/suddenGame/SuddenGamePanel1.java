package tycoongraduate.views.suddenGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class SuddenGamePanel1 extends JPanel{
	private MainFrame mf;
	private SuddenGamePanel1 panel = this;
	private BackgroundImage bg = new BackgroundImage();
	private JLabel suddenGameInfo = new JLabel();
	private DialogImage dialog = new DialogImage();
	private PlayerVo player;
	
	public void paintComponent(Graphics g) {
		//주인공이 서있는 배경 이미지 = images/miniGame/sudden_game/bground_0.jpg
		g.drawImage(bg.suddenBground().getImage(), 0, 0, 1400, 900, null);
		setOpaque(false); 
		super.paintComponent(g);
	}
	public SuddenGamePanel1(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		//돌발 게임 설명 이미지 = images/miniGame/sudden_game/info_60.jpg
		suddenGameInfo.setIcon(dialog.suddenDialogExplain());
		suddenGameInfo.setLayout(null);
		suddenGameInfo.setBounds(200, 100, 1000, 700);
		this.add(suddenGameInfo);
		suddenGameInfo.setVisible(true);
		suddenGameInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new SuddenGamePanel2(mf, player));
			}
		});
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new SuddenGamePanel2(mf, player));
			}
		});
	}
}