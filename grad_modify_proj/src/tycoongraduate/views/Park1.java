package tycoongraduate.views;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.common.ChangePanel;

public class Park1 extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	private MapImage map = new MapImage();
	private DialogImage dialog = new DialogImage();
	
	public Park1(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		this.addMouseListener(new clickPanel());
	}
	public void paintComponent(Graphics g) {
		if(player.getGender() == "M") {
			//남자 혼자 있는 배경 = images/parkMap/park.png
			g.drawImage(map.parkMap().getImage(), 0, 0, null);
		}else {
			//여자 혼자 있는 배경 = images/parkMap/parkW.png
			g.drawImage(map.parkWMap().getImage(), 0, 0, null);
		}
		//대화창 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1390, 250, null);
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("!!! 수상한 사람이다 !!!", 70, 750);
		//느낌표 = images/parkMap/exMark.png
		g.drawImage(map.mark().getImage(),900, 250, 154, 174, null);
		setOpaque(false); // 그림을 표시하게 설정,투명하게 조절
		super.paintComponent(g);
	}
	class clickPanel extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			ChangePanel.changePanel(mf, panel, new Park2(mf,player));
			mf.revalidate();
		}
	}
}