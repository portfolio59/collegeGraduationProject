package tycoongraduate.views;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.model.vo.image.MapImage;
import tycoongraduate.views.mainMap.MainMapSosok;
import tycoongraduate.views.common.ChangePanel;

public class Park2 extends JPanel {
	private MainFrame mf;
	private JPanel panel = this;
	private PlayerVo player;
	private MapImage map = new MapImage();
	private DialogImage dialog = new DialogImage();
	
	public Park2(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		this.setLayout(null);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new MainMapSosok(mf,player));
				mf.repaint();
			}
		});
	}
	public void paintComponent(Graphics g) {
		if(player.getGender() == "M") {
			//남자 혼자 있는 배경 = images/parkMap/park2.png
			g.drawImage(map.park2Map().getImage(), 0, 0, null);
		}else {
			//여자 혼자 있는 배경 = images/parkMap/parkW2.png
			g.drawImage(map.parkW2Map().getImage(), 0, 0, null);
		}
		//대화창 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1390, 250, null);		
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("수상한 사람 : 자네 트로트의 신이 되어 보지 않을 텐가?", 70, 730);
		g.drawString("관심이 있다면 내가 있는 JYB소속사로 오게나!", 290, 780);
		setOpaque(false); // 그림을 표시하게 설정,투명하게 조절
		super.paintComponent(g);
	}
}