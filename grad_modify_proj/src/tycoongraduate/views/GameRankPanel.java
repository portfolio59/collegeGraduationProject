package tycoongraduate.views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import tycoongraduate.model.controller.UserController;
import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.ending.EndingBack03;

public class GameRankPanel extends JPanel{
	private MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	private DialogImage dialog = new DialogImage();
	private ButtonImage btnImg = new ButtonImage();
	
	public GameRankPanel(MainFrame mf, PlayerVo player2) {
		this.mf = mf;
		panel = this;
		this.player = player2;
		this.setFocusable(true);
		
		//돌아가기 버튼 엔딩화면으로 다시 돌아간다
		JButton replayBtn = new JButton(new ImageIcon(btnImg.back_btn().getImage().getScaledInstance(200, 100, 0)));
		replayBtn.setLayout(null);
		replayBtn.setBorderPainted(false);
		replayBtn.setContentAreaFilled(false);
		replayBtn.setFocusPainted(false);
		replayBtn.setBounds(500,800,200,50);
		replayBtn.setText("다시하기");
		
		//여기다 db 조회해서 불러올거..
		ArrayList<PlayerVo> list = new UserController().selectAllUser(player);
		JTextArea rankPrint = new JTextArea();
		rankPrint.append("순위-유저아이디-총점-달성일-소감한마디\n");
		
		for(int i = 0; i < list.size(); i++){
			PlayerVo player = (PlayerVo) list.get(i);
			rankPrint.append(" " + (i+1) + "등-" + player.getUserId() + " 님-"+ player.getTotalScore() + " 점-" + player.getEndingDate()+ "-" + player.getEndingComment() +"\n");
			if(i >= 9){
				break;
			}
		}
		rankPrint.setBounds(80, 150, 1300, 600);
		rankPrint.setFont(new Font("Sanscerif", Font.BOLD, 40));
		rankPrint.setEditable(false);
		this.add(rankPrint);
		
		replayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new EndingBack03(mf, player));
				mf.revalidate();
			}
		});
		this.add(replayBtn);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(dialog.talkFormat().getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
		g.setColor(Color.WHITE);
	}
}
