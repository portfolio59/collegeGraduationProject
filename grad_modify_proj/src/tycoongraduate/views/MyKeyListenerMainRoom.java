package tycoongraduate.views;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.danceGame.MiniGameDance2;
import tycoongraduate.views.dispatchGame.DispatchGameFirstPanel;
import tycoongraduate.views.dispatchGame.DispatchGameSecondPanel;
import tycoongraduate.views.ending.EndingBack01;
import tycoongraduate.views.ending.EndingBack02;
import tycoongraduate.views.ending.EndingBack03;
import tycoongraduate.views.ending.EndingBackSudden;
import tycoongraduate.views.jungukGame.JungukThirdPanel;
import tycoongraduate.views.mainMap.MainMapFirst;
import tycoongraduate.views.sosokMap.Sosok_03_trot;
import tycoongraduate.views.mainMap.MainMapDispatch;
import tycoongraduate.views.suddenGame.SuddenGamePanel1;
import tycoongraduate.views.suddenGame.SuddenGamePanel2;
import tycoongraduate.views.suddenGame.SuddenGameStart;
import tycoongraduate.views.trotGame.TrotGameFirstPanel;
import tycoongraduate.views.trotGame.TrotGameSecondPanel;

public class MyKeyListenerMainRoom extends KeyAdapter{
	private MainFrame mf;
	private JPanel panel;
	private static final int MOVE_UNIT = 10;
	private JLabel la;
	private PlayerVo player;

	public MyKeyListenerMainRoom(JLabel la, MainFrame mf, JPanel panel, PlayerVo player) {
		this.la = la;
		this.mf = mf;
		this.panel = panel;
		this.player = player;
	}
	public void keyReleased(KeyEvent e) {
		if((la.getX() >= 560 && la.getX() <= 690)&&(la.getY() >= 500 && la.getY() <= 540)) {
			ChangePanel.changePanel(mf, panel, new MainMapFirst(mf,player));
			player.setTotalScore(800);
//			ChangePanel.changePanel(mf, panel, new DispatchGameSecondPanel(mf,player));
			mf.revalidate();
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		int a = la.getX();
		int b = la.getY();
		
		switch(keyCode) {

		case KeyEvent.VK_UP :
			if(b >= 150) {
			la.setLocation(la.getX(),la.getY()-MOVE_UNIT);
			}
			break;
		case KeyEvent.VK_DOWN :
			if(b <= 500) {
			la.setLocation(la.getX(), la.getY() + MOVE_UNIT);
			}
			break;
		case KeyEvent.VK_LEFT :
			if(a >= 320) {
			la.setLocation(la.getX() - MOVE_UNIT, la.getY());
			}
			break;
		case KeyEvent.VK_RIGHT :
			if(a <= 920) {
			la.setLocation(la.getX() + MOVE_UNIT, la.getY());
			}
			break;
		}
	}
}