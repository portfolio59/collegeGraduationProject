package tycoongraduate.views;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.views.common.ChangePanel;

public class SelectCharacter extends JPanel {
	private MainFrame mf;
	private SelectCharacter panel = this;
	private JLabel label;
	private JLabel label2;
	private PlayerVo player;
	private ButtonImage btnImg = new ButtonImage();
	
	public void paintComponent(Graphics g) {	
		//빨간 커튼 배경 이미지 불러오기
		BackgroundImage bg = new BackgroundImage();
		g.drawImage(bg.characterSelecBg().getImage(), 0, 0, 1400, 900, null);
		mf.repaint();
		setOpaque(false); 
		super.paintComponent(g);
		mf.repaint();
	}
	public SelectCharacter(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		label = new JLabel();
		//츄리닝 복장의 남자 케릭터 = images/chracters/CharacterM.gif
		CharacterImage character = new CharacterImage();
		Image imageM = character.characterMgifbf().getImage();
		label.setIcon(new ImageIcon(imageM));
		label2 = new JLabel();
		//츄리닝 복장의 여자 케릭터 = images/chracters/CharacterW.gif
		Image imageW = character.characterWgifbf().getImage();
		label2.setIcon(new ImageIcon(imageW));
		label.setBounds(200,50,450,700);
		label2.setBounds(800,50,450,700);
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				player.setGender("M");
				ChangePanel.changePanel(mf, panel, new MainRoom(mf,player));
				mf.validate();
			}
		});
		label2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				player.setGender("W");
				ChangePanel.changePanel(mf, panel, new MainRoom(mf,player));
				mf.validate();
			}
		});
		panel.add(label);
		panel.add(label2);
		
		//돌아가기 버튼 닉네임 선택창으로 다시 돌아간다
		JButton replayBtn = new JButton(new ImageIcon(btnImg.back_btn().getImage().getScaledInstance(200, 100, 0)));
		replayBtn.setLayout(null);
		replayBtn.setBorderPainted(false);
		replayBtn.setContentAreaFilled(false);
		replayBtn.setFocusPainted(false);
		replayBtn.setBounds(600,800,200,50);
		replayBtn.setText("돌아가기");
		replayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new NickName(mf));
				mf.revalidate();
			}
		});
		this.add(replayBtn);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new GameStartPanel(mf));
				mf.validate();
			}
		});
	}
}	
