package tycoongraduate.views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.common.ChangePanel;

public class NickName extends JPanel {
   public static String nickname;
   private NickName nick = this;
   private PlayerVo player;

   public void paintComponent(Graphics g) { 
	   BackgroundImage bg = new BackgroundImage();
	   g.drawImage(bg.redBground().getImage(), 0, 0, 1400, 900, null);
	   setOpaque(false); 
	   super.paintComponent(g);
   };
   public NickName(MainFrame mf) {
	  player = new PlayerVo();
      JTextField get_nickname = new JTextField(3) {
         public void paintComponent (Graphics g) {
        	//내용없는 대화창 이미지 불러오기
        	DialogImage dialog = new DialogImage();
            g.drawImage(dialog.talkFormat().getImage(), 0, 0, 450, 120, null);
            setOpaque(false);
          super.paintComponent(g);
         }
      };
      Font neo = new Font("Neo둥근모",Font.BOLD,30);
      get_nickname.setLayout(null);
      get_nickname.setBorder(null);
      get_nickname.setHorizontalAlignment(JTextField.CENTER);
      get_nickname.setFont(neo);   
      get_nickname.setBounds(540,350,480,120);
      get_nickname.setOpaque(false);
      this.add(get_nickname);
      JLabel nn = new JLabel() {
    	 DialogImage dialog = new DialogImage();
         public void paintComponent (Graphics g) {
            g.drawImage(dialog.nickn().getImage(), 0, 0, 300, 100, null);
            setOpaque(false);
            super.paintComponent(g);
         }
      };
      nn.setLayout(null);
      nn.setBounds(190,350,400,100);
      this.add(nn);

      JTextField setBtnText = new JTextField();
      setBtnText.setBounds(450, 100, 700, 100);
      setBtnText.setEditable(false);
      this.add(setBtnText);
      Font neo2 = new Font("Neo둥근모",Font.BOLD, 25);
      setBtnText.setForeground(Color.WHITE);
      setBtnText.setFont(neo2);
      setBtnText.setBorder(null);
      setBtnText.setOpaque(false);
      //이미지아이콘 불러오기 
      ButtonImage butn = new ButtonImage();
      //select 박스이미지 = images/select.png
      Image select_btn = butn.select_btn().getImage();
      JButton btn = new JButton(new ImageIcon(select_btn));
      btn.setBounds(550,650,300,50);
      btn.setBorderPainted(false);
      btn.setContentAreaFilled(false);
      btn.setFocusPainted(false);
      this.add(btn);
      btn.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            nickname = get_nickname.getText();
            if (nickname.equals("")) {
            	 setBtnText.setText("닉네임을 3글자이하로 적어주세요~");
            }else {
            setBtnText.setText("트롯스타 메이커에 오신걸 환영합니다. "+ nickname + " 님");
            setBtnText.removeAll();
            setBtnText.setFocusable(true);
            player.setUserId(nickname);
            ChangePanel.changePanel(mf,nick,new SelectCharacter(mf,player));
            }
         }
      });
   }
}