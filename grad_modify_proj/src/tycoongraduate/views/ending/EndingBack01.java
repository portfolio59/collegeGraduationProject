package tycoongraduate.views.ending;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.EndingImage;
import tycoongraduate.views.GameRankPanel;
import tycoongraduate.views.GameStartPanel;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class EndingBack01 extends JPanel { //0~50% - 방구석 스타 엔딩..
	private MainFrame mf;
	private EndingBack01 panel = this;
	private PlayerVo player;
	private EndingImage endingImg = new EndingImage();
	private ButtonImage btnImg = new ButtonImage();
	
	public EndingBack01(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		this.setFocusable(true);
		//돌아가기 버튼 닉네임 선택창으로 다시 돌아간다
		JButton replayBtn = new JButton(new ImageIcon(btnImg.replay_btn().getImage().getScaledInstance(200, 100, 0)));
		replayBtn.setLayout(null);
		replayBtn.setBorderPainted(false);
		replayBtn.setContentAreaFilled(false);
		replayBtn.setFocusPainted(false);
		replayBtn.setBounds(500,800,200,50);
		replayBtn.setText("다시하기");
		
		replayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new GameStartPanel(mf));
				mf.revalidate();
			}
		});
		this.add(replayBtn);
		
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(endingImg.ending_bottom().getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
		g.setColor(Color.WHITE);
		g.setFont(new Font("Neo둥근모",Font.BOLD,30));
		g.drawString("당신의 스토리 합산 총점은 " + player.getTotalScore() + "점 입니다.", 400, 50);
		g.drawString("800점 이상~                       월드 스타", 900, 100);
		g.drawString("550점 이상~ 800점 미만   무명 스타", 900, 150);
		g.drawString("0점 이상~ 550점 미만   방구석 스타", 900, 200);
	}
}