package tycoongraduate.views.ending;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.EndingImage;
import tycoongraduate.views.GameRankPanel;
import tycoongraduate.views.GameStartPanel;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class EndingBack03 extends JPanel { //76~100% - 월드 스타 엔딩..
	private EndingBack03 panel = this;
	private MainFrame mf;
	private PlayerVo player;
	private PlayerVo newPlayer;
	private EndingImage endingImg = new EndingImage();
	private ButtonImage btnImg = new ButtonImage();
	private CharacterImage character = new CharacterImage();

	public EndingBack03(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		//돌아가기 버튼 닉네임 선택창으로 다시 돌아간다
		JButton replayBtn = new JButton(new ImageIcon(btnImg.back_btn().getImage().getScaledInstance(200, 100, 0)));
		replayBtn.setLayout(null);
		replayBtn.setBorderPainted(false);
		replayBtn.setContentAreaFilled(false);
		replayBtn.setFocusPainted(false);
		replayBtn.setBounds(500,800,200,50);
		replayBtn.setText("다시하기");
		
		replayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new GameStartPanel(mf));
				mf.revalidate();
			}
		});
		this.add(replayBtn);
		
		//명예의 전당 버튼
		JButton regendBtn = new JButton(new ImageIcon(btnImg.hallOfFame_btn().getImage().getScaledInstance(200, 100, 0)));
		regendBtn.setLayout(null);
		regendBtn.setBorderPainted(false);
		regendBtn.setContentAreaFilled(false);
		regendBtn.setFocusPainted(false);
		regendBtn.setBounds(800,800,200,50);
		regendBtn.setText("명예의 전당 보기");
		regendBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new GameRankPanel(mf, player));
			}
		});
		this.add(regendBtn);
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(endingImg.ending_top().getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
		g.setColor(Color.WHITE);
		g.setFont(new Font("Neo둥근모",Font.BOLD,30));
		g.drawString("당신의 스토리 합산 총점은 " + player.getTotalScore() + "점 입니다.", 400, 50);
		g.drawString("800점 이상~                       월드 스타", 900, 100);
		g.drawString("550점 이상~ 800점 미만   무명 스타", 900, 150);
		g.drawString("1점 이상~ 550점 미만   방구석 스타", 900, 200);
		if(player.getGender() == "M") {
			g.drawImage(character.endingCharacterMgif().getImage(), 400, 100, 600, 600, this);
		}else {
			g.drawImage(character.endingCharacterWgif().getImage(), 400, 100, 600, 600, this);
		}
	}
}