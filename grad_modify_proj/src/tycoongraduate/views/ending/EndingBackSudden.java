package tycoongraduate.views.ending;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.EndingImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.MainRoom;
import tycoongraduate.views.common.ChangePanel;

public class EndingBackSudden extends JPanel { 
	// 돌발 엔딩...돌발 미션에 실패했을경우
	private MainFrame mf;
	private EndingBackSudden panel = this;
	private EndingImage endingImg = new EndingImage();
	private PlayerVo player;
	private PlayerVo newPlayer = new PlayerVo();
	private ButtonImage btnImg = new ButtonImage();
	
	public EndingBackSudden(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		//돌아가기 버튼 닉네임 선택창으로 다시 돌아간다
		JButton replayBtn = new JButton(new ImageIcon(btnImg.replay_btn().getImage().getScaledInstance(200, 100, 0)));
		replayBtn.setLayout(null);
		replayBtn.setBorderPainted(false);
		replayBtn.setContentAreaFilled(false);
		replayBtn.setFocusPainted(false);
		replayBtn.setBounds(900,770,200,50);
		replayBtn.setText("다시하기");
		newPlayer.setUserId(player.getUserId());
		newPlayer.setGender(player.getGender());
		replayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePanel.changePanel(mf, panel, new MainRoom(mf, newPlayer));
				mf.revalidate();
			}
		});
		this.add(replayBtn);
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(endingImg.ending_sudden().getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
		g.setColor(Color.WHITE);
	}
}