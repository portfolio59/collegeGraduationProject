package tycoongraduate.views.dispatchGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.*;

import tycoongraduate.model.vo.game.DispatchGameScoreAvoidVo;
import tycoongraduate.model.vo.game.DispatchGameScoreHeartVo;
import tycoongraduate.model.vo.game.DispatchGameScoreStarVo;
import tycoongraduate.model.controller.UserController;
import tycoongraduate.model.vo.game.DispatchGameImagesVo;
import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.ending.EndingBack01;
import tycoongraduate.views.ending.EndingBack02;
import tycoongraduate.views.ending.EndingBack03;

public class DispatchGameSecondPanel extends JPanel implements KeyListener{
	private static final int SCREEN_WIDTH = 1400;
	private static final int SCREEN_HEIGHT = 900;
	private MainFrame mf;
	private JPanel panel;
	private JLabel label;
	private PlayerVo player;
	//플레이어 초기 위치
	int x = 100 ;
	int y = 100 ;
	//버퍼 이미지로 움직이는 거같은 배경만들기에 필요한 변수
	int bx = 0;

	boolean KeyUp = false;
	boolean KeyDown = false;
	boolean KeyLeft = false;
	boolean KeyRight = false;
	boolean KeySpace = false;
	
	boolean stopAll = false;
	//별 점수 스레드에서 별 점수 개수를 조절하는 변수
	private int cnt_star = 0;
	//하트 점수 스레드에서 하트 점수 개수를 조절하는 변수
	private int cnt_heart = 0;
	//디스패치 (피해야하는)점수 스레드에서 cnt가 100개당  디스패치 개수를 조절하는 변수
	private int cnt_avoid = 0;
	//점수들 이동 속도 조절 변수
	private int avoid_Speed;
	private int star_Speed;
	private int heart_Speed;
	
	private int game_Score = 0;

	private Timer timer;
	//남은 시간 값 변수 = 30초로 고정
	private int timeLeft = 30;

	private JLabel timeLeftLabel;
	private JLabel totalScoreLabel;
	//player, star, heart 마다 각각의 쓰레드 생성
	private Thread thAvoid;
	private Thread thStar;
	private Thread thHeart;
	//각각의 이미지 생성(배경,플레이어,점수들)
	private Image bg_img;           
	private Image player_img;        
	private Image dispatch_img;         
	private Image heart_img;         
	private Image star_img;            
	
	ArrayList<DispatchGameScoreAvoidVo> Score_List = new ArrayList<DispatchGameScoreAvoidVo>();
	ArrayList<DispatchGameScoreHeartVo> Scoreh_List = new ArrayList<DispatchGameScoreHeartVo>();
	ArrayList<DispatchGameScoreStarVo> Scores_List = new ArrayList<DispatchGameScoreStarVo>();
	//버퍼 이미지 생성 변수
	Image buffImage;
	//그림을 그려주는 상위객체
	Graphics buffg;
	//점수들의 vo 변수
	DispatchGameScoreAvoidVo sc_avoidVo;
	DispatchGameScoreHeartVo sc_heartVo;
	DispatchGameScoreStarVo sc_starVo;
	private DispatchGameImagesVo dgv = new DispatchGameImagesVo();
	private CharacterImage character = new CharacterImage();
	private DialogImage dialog = new DialogImage();
	private UserController member = new UserController();;
	
	public DispatchGameSecondPanel (MainFrame mf, PlayerVo player) {
//		System.out.println("dispatchGamePanel class player.getuserId() : " + player.getUserId());
		this.mf = mf;
		panel = this;
		this.player = player;
		init();
		start();
		setFocusable(true);
		KeyProcess();
	}
	public void init() {
		if(player.getGender() == "M") {
			//변신 된 남자 트로트스타 동적 케릭터 gif 이미지 = images/chracters/FCharacterM.gif
			player_img = character.characterMgifaf().getImage().getScaledInstance(80, 80, 0);
		} else if(player.getGender() == "W") {
			//변신 된 여자 트로트스타 동적 케릭터 gif 이미지 = images/chracters/FCharacterW.gif
			player_img = character.characterWgifaf().getImage().getScaledInstance(80, 80, 0);
		}
		//디스패치 게임 피하기(장식) 이미지 = images/miniGame/dispatch_game/score_avoid.png
		dispatch_img = dgv.scoreAvoidImg().getImage().getScaledInstance(80, 80, 0);
		//디스패치 게임 하트먹기(장식) 이미지 = images/miniGame/dispatch_game/score_heart.png
		heart_img = dgv.scoreHeartImg().getImage().getScaledInstance(80, 80, 0);
		//디스패치 게임 별먹기(장식) 이미지 = images/miniGame/dispatch_game/score_star.png
		star_img = dgv.scoreStarImg().getImage().getScaledInstance(80, 80, 0);
		//디스패치 게임 배경화면 이미지 = images/miniGame/dispatch_game/background.png
		bg_img = dgv.dispatchGameBgImg().getImage().getScaledInstance(SCREEN_WIDTH, SCREEN_HEIGHT, 0);

		game_Score = player.getTotalScore();
		//점수들 속도 조절
		avoid_Speed = 2;
		heart_Speed= 2;
		star_Speed = 2;
		//대화창 틀 형식 이미지 = images/dialog_format.png
		totalScoreLabel = new JLabel(new ImageIcon(dialog.talkFormat().getImage().getScaledInstance(800, 400, java.awt.Image.SCALE_SMOOTH)));
		totalScoreLabel.setLayout(null);
		totalScoreLabel.setBounds(300,0,1000,1000);
		totalScoreLabel.setVisible(false);
		this.add(totalScoreLabel);
		revalidate();

		timeLeftLabel = new JLabel("시간 : ");
		timeLeftLabel.setHorizontalAlignment(SwingConstants.CENTER);
		timeLeftLabel.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		timeLeftLabel.setForeground(Color.WHITE);
		timeLeftLabel.setBounds(100,20,250,200);
		this.add(timeLeftLabel);
		revalidate();
	}
	public void start() {
		addKeyListener(this);
		KeyProcess();
		thAvoid = new avoidThread();
		thAvoid.start();
		repaint();

		thStar = new starThread();
		thStar.start();
		repaint();

		thHeart = new heartThread();
		thHeart.start();
		repaint();
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(timeLeft == 0) {
					timeLeftLabel.setText("남은 시간  : ");
					timer.stop();
					stopAll = true;
					player.setTotalScore(player.getTotalScore() + game_Score);
					if(player.getTotalScore() >= 800) {
						//총 점수가 500점 이상이면 월드 스타 엔딩
						String comment = JOptionPane.showInputDialog("월드 스타가 된 소감을 입력해주세요!");
						player.setEndingComment(comment);
						//월드스타가 된 유저들을 DB에 등록
						member.userStoredDb(player);
						ChangePanel.changePanel(mf, panel, new EndingBack03(mf, player));
						mf.revalidate();
					}else if(player.getTotalScore() >= 550 && player.getTotalScore() < 800) {
						//총 점수가 250점 이상 499점 이하면 무명 스타 엔딩
						ChangePanel.changePanel(mf, panel, new EndingBack02(mf, player));
						mf.revalidate();
					}else if(player.getTotalScore() >= 1 && player.getTotalScore() < 550) {
						//총 점수가 2점 이상 250점 미만이면 방구석 스타 엔딩
						ChangePanel.changePanel(mf, panel, new EndingBack01(mf, player));
						mf.revalidate();
					}	
				}
				timeLeftLabel.setText("남은 시간 : " + timeLeft);
				timeLeft--;
			}
		});
		timer.start();
		if(game_Score < 0 || timeLeft == 0) {
			timer.stop();
			stopAll = true;			
		}
	}
	//각각의 스레드
	class starThread extends Thread{
		public starThread() {
		}
		@Override
		public void run() {
			try{
				while(true) {
					if(!(stopAll)) {
					repaint();
					cnt_star++;
//					System.out.println("cnt_star : " + cnt_star);
					sleep(10);
					starProcess();
					}
				}
			} catch(Exception e) {
				System.out.println("starThread 오류발생");
				e.printStackTrace();
			}
		}
		public void starProcess() {
			for(int i = 0; i < Scores_List.size(); ++i) {
				sc_starVo = (DispatchGameScoreStarVo) (Scores_List.get(i));
				sc_starVo.move();
				if(sc_starVo.getX() < -200) {
					Scores_List.remove(i);
				}
				if(Crash(x,y, sc_starVo.getX(), sc_starVo.getY(), player_img, star_img)) {
					Scores_List.remove(i);  
					game_Score += 20;      
					System.out.println("game_score 별을 먹었다 : " + game_Score);
				}
				if(game_Score < 0) {
					return;
				}
			}
			if(cnt_star % 200 == 0) {
				for(int m = (int)(Math.random()*900 + 1); m < (SCREEN_HEIGHT -80); m+=500) {
					sc_starVo = new DispatchGameScoreStarVo(SCREEN_WIDTH + 
							(int)(Math.random()*70 + 1), (int)(Math.random()*720 + 1), star_Speed + (int)(Math.random()*7 + 3));
					Scores_List.add(sc_starVo);
				}
			}
		}
	}
	class heartThread extends Thread{
		public heartThread() {
		}
		@Override
		public void run() {
			try{
				while(true) {
					if(!(stopAll)) {
					repaint();
					cnt_heart++;
					Thread.sleep(10);
					heartProcess();
					}
				}
			} catch(Exception e) {
				System.out.println("heartThread 오류!");
				e.printStackTrace();
			}
		}
		public void heartProcess() {
			for(int i = 0; i < Scoreh_List.size(); ++i) {
				sc_heartVo = (DispatchGameScoreHeartVo) (Scoreh_List.get(i));
				sc_heartVo.move();
				if(sc_heartVo.getX() < -200) {
					Scoreh_List.remove(i);
				}
				if(Crash(x,y, sc_heartVo.getX(), sc_heartVo.getY(), player_img, heart_img)) {
					Scoreh_List.remove(i);   
					game_Score += 20;      
					System.out.println("game_score 하트를 먹었다 : " + game_Score);
				}
				if(game_Score < 0) {
					return;
				}
			}
			if(cnt_heart % 200 == 0) {
				for(int k = (int)(Math.random()*720+ 1); k < (SCREEN_HEIGHT -80); k+=(int)(Math.random()*720+ 1)) {
					sc_heartVo = new DispatchGameScoreHeartVo(SCREEN_WIDTH + 
							(int)(Math.random()*70 + 1),(int)(Math.random()*780+ 1), heart_Speed + (int)(Math.random()*7+ 3));
					Scoreh_List.add(sc_heartVo);
				}
			}
		}
	}
	class avoidThread extends Thread {
		public avoidThread() {}
		@Override
		public void run() {
			try{
				while(true) {
					if(!(stopAll)) {
					KeyProcess();      
					repaint();
					cnt_avoid++;
					Thread.sleep(30);
					avoidProcess();
					}
				}
			} catch(Exception e) {
				System.out.println("run() 실패오류");
				e.printStackTrace();
			}
		}
		public void avoidProcess() {
			for(int i = 0; i < Score_List.size(); ++i) {
				sc_avoidVo = (DispatchGameScoreAvoidVo) (Score_List.get(i));
				sc_avoidVo.move();
				if(sc_avoidVo.getX() < -200 ) {
					Score_List.remove(i);
				}
				if (Crash(x, y, sc_avoidVo.getX(), sc_avoidVo.getY(), player_img, dispatch_img)) {
					Score_List.remove(i);  
					game_Score -= 30;     
					System.out.println("game_score 점수를 잃었다 : " + game_Score);
				}
				if(game_Score < 0) {
					stopAll = true;			
					timer.stop();
					ChangePanel.changePanel(mf, panel, new EndingBack01(mf, player));
				}            
			}
			if(cnt_avoid % 100 == 0) {
				for(int j = (int)(Math.random()*720+ 1); j < (SCREEN_HEIGHT -20); j+=45) {
					sc_avoidVo = new DispatchGameScoreAvoidVo(SCREEN_WIDTH + 
							(int)(Math.random()*500 + 1), (int)(Math.random()*720+ 1), avoid_Speed +(int)(Math.random()*7+ 3));
					Score_List.add(sc_avoidVo);
				}
			}
		}
	}
	//충돌 판정 메소드
	public boolean Crash(int x1, int y1, int x2, int y2, Image player_img, Image dispatch_img) {
		boolean check = false;
		//crash로 전달받은 플레이어의 x,y 좌표와 (=x1,y1) 점수의 좌표(=x2,y2) 얻어내서
		//플레이어의 x좌표와 플레이어 이미지의 가로길이를 덧셈하여 점수의 x좌표와 점수 이미지의 가로길이를 덧셈한 결과를 빼는 것이
		//플레이어 가로이미지와 점수 이미지의 가로길이를 더한것보다 작아야함
		// 이미지 변수를 바로 받아 해당 이미지의 넓이, 높이값을 바로 계산
		if(Math.abs((x1 + player_img.getWidth(null) / 2) - (x2 + dispatch_img.getWidth(null) / 2)) <
				(dispatch_img.getWidth(null) / 2 + player_img.getWidth(null) / 2) 
				&& Math.abs((y1 + player_img.getHeight(null) / 2)  - (y2 + dispatch_img.getHeight(null) /2)) <
				(dispatch_img.getHeight(null) / 2 + player_img.getHeight(null) /2 )) {
			check = true;   
		} else {
			check = false;
		}
		return check;      
	}
	public void paint() {}
	public void paint(Graphics g) {
		setFocusable(true);
		requestFocus();
		buffImage = createImage(SCREEN_WIDTH, SCREEN_HEIGHT);
		buffg = buffImage.getGraphics();
		update(g);
	}
	public void update() {}
	public void update(Graphics g) {
		Draw_Background();
		Draw_Player();
		Draw_Score();
		Draw_StatusText();
		Draw_Timer();
		g.drawImage(buffImage, 0, 0, this);      
	}
	public void Draw_Timer() {
		buffg.drawString("남은 시간 : " + timeLeft +"초",700, 70);
	}
	public void Draw_Background() {
		if(game_Score >= 0) {
			//화면 지우기 명령
			buffg.clearRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
			if(bx> - 1400) {
				buffg.drawImage(bg_img, bx, 0, this);
				bx -= 1;
				//bx를 0에서 -1만큼 계속 줄이므로 배경이미지의 x좌표는
				//계속 좌측으로 이동 그러므로 전체 배경은 천천히 
				//좌측으로 이동하게 됨.
				buffg.drawImage(bg_img, bx+1401, 0, this);
			} else {
				bx = 0;
			}
		} else {
			buffg.drawImage(bg_img, bx, 0, this);
			buffg.drawImage(bg_img, bx+1399, 0, this);
		}
	}
	public void Draw_Player() {
		buffg.drawImage(player_img, x, y, this);
	}
	public void Draw_StatusText() {
		buffg.setFont(new Font("serif", Font.BOLD, 20));
		buffg.drawString("획득 점수 : "  + game_Score, 1000, 70);
	}
	public void Draw_Score() {
		for (int i = 0; i < Score_List.size(); ++i) {
			sc_avoidVo = (DispatchGameScoreAvoidVo) (Score_List.get(i));
			//buffg.drawImage의 3번재 인자는 observer로 더 많은 이미지가 변환될 때 알림을 받을 개체를 설정해주는 것이다.
			buffg.drawImage(dispatch_img, sc_avoidVo.getX(),   sc_avoidVo.getY(), this);
		}
		for ( int i= 0; i < Scoreh_List.size(); ++i) {
			sc_heartVo = (DispatchGameScoreHeartVo) (Scoreh_List.get(i));
			buffg.drawImage(heart_img, sc_heartVo.getX(), sc_heartVo.getY(), this);
		}
		for ( int i= 0; i < Scores_List.size(); ++i) {
			sc_starVo = (DispatchGameScoreStarVo) (Scores_List.get(i));
			buffg.drawImage(star_img, sc_starVo.getX(), sc_starVo.getY(), this);
		}
	}
	//키가 눌렸을때 이동할수 있도록 조절
	public void KeyProcess() {
		if(KeyUp == true) {
			if (y > 20)
				y -= 15;              
		}
		if(KeyDown == true) {
			if (y + player_img.getHeight(null)+40 < SCREEN_HEIGHT)
				y += 15;               
		}
		if(KeyLeft == true) {
			if(x > 0)
				x -= 15;               
		}
		if(KeyRight == true) {
			if(x + player_img.getWidth(null) < SCREEN_WIDTH)
				x += 15;               
		}
		if(KeySpace == true) {
			System.out.println( x+ ", " + y);
		}
	}
	//방향키를 눌렀을때만 true가 되도록 방향키를 눌렀을 때 true로 변경
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_UP :
			KeyUp = true;
			break;
		case KeyEvent.VK_DOWN :
			KeyDown = true;
			break;
		case KeyEvent.VK_LEFT :
			KeyLeft = true;
			break;
		case KeyEvent.VK_RIGHT :
			KeyRight = true;
			break;
		case KeyEvent.VK_SPACE :
			KeySpace = true;
			break;
		}
	}
	//방향키를 눌렀을때만 true가 되도록 방향키를 뗐을때 false로 변경
	@Override
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_UP :
			KeyUp = false;
			break;
		case KeyEvent.VK_DOWN :
			KeyDown = false;
			break;
		case KeyEvent.VK_LEFT :
			KeyLeft = false;
			break;
		case KeyEvent.VK_RIGHT :
			KeyRight = false;
			break;
		case KeyEvent.VK_SPACE :
			KeySpace = false;
			break;
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {
	}
}