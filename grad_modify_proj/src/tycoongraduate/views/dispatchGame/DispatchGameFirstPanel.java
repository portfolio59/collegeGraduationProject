package tycoongraduate.views.dispatchGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.DispatchGameImagesVo;
import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class DispatchGameFirstPanel extends JPanel{
	private MainFrame mf;
	private DispatchGameFirstPanel panel;
	private JLabel label;
	private DispatchGameImagesVo dgv = new DispatchGameImagesVo();
	private DialogImage dialog = new DialogImage();
	private PlayerVo player;
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//디스패치 게임 배경화면 이미지 = images/miniGame/dispatch_game/background.png
		g.drawImage(dgv.dispatchGameBgImg().getImage(), 0 , 0 ,1400,900,this);
		setOpaque(false);
	}
	public DispatchGameFirstPanel(MainFrame mf, PlayerVo player) {
		panel = this;
		this.mf = mf;
		this.player = player;
		//디스패치 게임 설명 이미지 = images/miniGame/dispatch_game/dispatch_game_dialog.png
		label = new JLabel(dialog.dispatchDialogExplain());
		panel.setLayout(null);
		label.setBounds(200,50,1000,700);
		label.setVisible(true);
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new DispatchGameSecondPanel(mf,player));
				mf.validate();
			}
		});
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new DispatchGameSecondPanel(mf,player));
				mf.validate();
			}
		});
		panel.add(label);
	}
}