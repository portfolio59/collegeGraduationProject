package tycoongraduate.views.singGame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;

public class MiniGameSsing1 extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private DialogImage dialog = new DialogImage();
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private PlayerVo player;
	public MiniGameSsing1(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MiniGameSsing2 gp = new MiniGameSsing2(mf,player);
				ChangePanel.changePanel(mf, panel, gp);
			}
		});
	}
	public void paintComponent(Graphics g) {
		//노래연습 배경 이미지 - images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0, null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1390, 250, null);
		if(player.getGender() == "M") {
			//추리닝 남자 동적이미지 = images/chracters/CharacterM.gif, null이 아닐때 실행?
			g.drawImage(character.characterMgifbf().getImage(), 50, 200, 398, 398, this);
		} else {
			//추리닝 여자 동적이미지 = images/chracters/CharacterW.gif, null이 아닐때 실행?
			g.drawImage(character.characterWgifbf().getImage(), 50, 200, 398, 398, this);
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("오늘도 노래 연습을 신나게 해보자!", 70, 750);
		mf.validate();
		setOpaque(false);
		super.paintComponent(g);
	}
}