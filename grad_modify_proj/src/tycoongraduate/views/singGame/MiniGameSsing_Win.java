package tycoongraduate.views.singGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.sosokMap.Sosok_dancebutton;
import tycoongraduate.views.sosokMap.Sosok_junguk;

public class MiniGameSsing_Win extends JPanel{
	private MainFrame mf;
	private MiniGameSsing_Win panel = this;
	private BackgroundImage bg = new BackgroundImage();
	private CharacterImage character = new CharacterImage();
	private DialogImage dialog = new DialogImage();
	private PlayerVo player;

	public MiniGameSsing_Win(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		this.setLayout(null);
		if(player.getSingScore() == 1 && player.getDanceScore() == 1) {
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					ChangePanel.changePanel(mf, panel, new Sosok_junguk(mf,player));
					mf.revalidate();
				}
			});
		}else {
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					ChangePanel.changePanel(mf, panel, new Sosok_dancebutton(mf,player));  
					mf.revalidate();
				}
			});
		}
	}
	public void jumsu(int jum) {
		int jum2 = jum;
		JLabel jlabel = new JLabel("노래능력이 "+jum2+"점 올랐다!");
		player.setSingScore(player.getSingScore() + jum2);
		jlabel.setBounds(5,5,300,50);
		jlabel.setFont(new Font("Sanscerif",Font.BOLD,20));
		jlabel.setForeground(Color.BLACK);
		panel.add(jlabel);
	}
	public void paintComponent(Graphics g) {
		//소속사 배경 이미지 = images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0, null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1400, 250, null);
		//win 크아 이미지 = images/miniGame/lose.png
		g.drawImage(dialog.winDialog().getImage(),350,50,700,170,null);
		if(player.getGender() == "M") {
			//추리닝 남자아이  gif 이미지 = images/characters/CharacterM.gif
			g.drawImage(character.characterMgifbf().getImage(), 500, 250, 398, 398, this);
		} else {
			//추리닝 여자아이  gif 이미지 = images/characters/CharacterW.gif
			g.drawImage(character.characterWgifbf().getImage(), 500, 250, 398, 398, this);
		}
//		if (character.characterMgifbf() != null) {
//			g.drawImage(character.characterMgifbf().getImage(), 500, 250, 398, 398, this);
//		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("오예~! 노래 능력치가 올랐다!!", 70, 750);
		setOpaque(false);
		super.paintComponent(g);
	}
}