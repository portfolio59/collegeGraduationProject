package tycoongraduate.views.singGame;

import javax.swing.JPanel;
import javax.swing.JTextField;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.common.Timer2;

public class MiniGameSsing_Test {
	private MainFrame mf;
	private JPanel panel;
	private JTextField input; 
	private JTextField input2;
	private JTextField input3;
	private int cnt = 0;
	private int num;
	//1라운드당 각 빈칸이 정답인가 확인하는 변수
	private boolean fone = false;
	private boolean ftwo = false;
	private boolean fthree = false;
	private Thread thread;
	private PlayerVo player;
	private Timer2 timer = new Timer2(mf,panel, player);
	private int score = 0;
	private Thread t1= timer;
	
	public MiniGameSsing_Test(MainFrame mf, JPanel panel, int num, JTextField input, JTextField input2, JTextField input3, PlayerVo player) {
		this.mf = mf;
		this.panel = panel;
		this.num = num;
		this.input = input;
		this.input2 = input2;
		this.input3 = input3;
		this.player = player;
	}

	public MiniGameSsing_Test(int jum) {
		this.score = jum;		
	}
	public void ssingNext() {
		MiniGameSsing_Win gp = new MiniGameSsing_Win(mf, player);
		//점수이 올랐다는 것을 왼쪽위 모서리에 표시
		gp.jumsu(score);
		ChangePanel.changePanel(mf, panel, gp);
		stop();
	}	
	public void cancel() {
		timer= new Timer2(mf, panel, player);
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timer.interrupt();
	}
	public void ssingNext_lose(int cnt2) {
		System.out.println("MiniGameSsing_text클래스에서 ssingNext_lose메소드안의 player : " + player);
		MiniGameSsing_Lose gp = new MiniGameSsing_Lose(mf,player,score, cnt2);
		gp.jumsu(score);
		ChangePanel.changePanel(mf, panel, gp);
		stop();
	}
	public final void stop() {
		timer= new Timer2(mf, panel, player);
		timer.interrupt();
	}
	public void StestPass(String[] inputArr) {
		String[][] answers = {{"얼굴", "몸매", "모든것이"}, 
							{"당신","나는","배터리"},
							{"당신", "동반자", "선물"},
							{"가인","가인","가인"},
							{"뭐야뭐야","뭐야","내버려"},
							{"유행가","나도","쿵쿵"},
							{"숫자","진짜","가슴"},
							{"땡벌","지쳤어요","길어요"},
							{"만드레","나는","향기속에"},
							{"오늘","사랑","헤어지면"}		
		};
		switch (num) {
		case 1:
			if (answers[0][0].equals(inputArr[0])) {
				System.out.println("input[1-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[0][1].equals(inputArr[1])){
				System.out.println("cnt1-2 :"+cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[0][2].equals(inputArr[2])) {
				System.out.println("cnt1-3 :"+cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 2:
			if (answers[1][0].equals(inputArr[0])) {
				System.out.println("input[2-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[1][1].equals(inputArr[1])) {
				System.out.println("cnt2-2 :"+cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[1][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 3:
			if (answers[2][0].equals(inputArr[0])) {
				System.out.println("input[3-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[2][1].equals(inputArr[1])) {
				System.out.println("cnt3-2 :"+cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[2][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 4:
			if (answers[3][0].equals(inputArr[0])) {
				System.out.println("input[4-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println("cnt4-2 :"+cnt);
			}
			if (answers[3][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[3][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 5:
			if (answers[4][0].equals(inputArr[0])) {
				System.out.println("input[5-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[4][1].equals(inputArr[1])) {
				System.out.println("cnt5-2 :"+cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[4][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 6:
			if (answers[5][0].equals(inputArr[0])) {
				System.out.println("input[6-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[5][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[5][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 7:
			if (answers[6][0].equals(inputArr[0])) {
				System.out.println("input[7-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[6][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[6][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 8:
			if (answers[7][0].equals(inputArr[0])) {
				System.out.println("input[8-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[7][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[7][2].equals(inputArr[2])){
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 9:
			if(answers[8][0].equals(inputArr[0])) {
				System.out.println("input[9-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[8][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[8][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println(cnt);
			break;
		case 10:
			if (answers[9][0].equals(inputArr[0])) {
				System.out.println("input[10-1]값 확인 : "+input.getText());
				if (!fone) {
					cnt++;
					fone = true;
				}
				System.out.println(cnt);
			}
			if (answers[9][1].equals(inputArr[1])) {
				System.out.println(cnt);
				if (!ftwo) {
					cnt++;
					ftwo = true;
				}
			}
			if (answers[9][2].equals(inputArr[2])) {
				System.out.println(cnt);
				if (!fthree) {
					cnt++;
					fthree = true;
				}
			}
			System.out.println("cnt" + cnt);
			break;
		}
		//통과인지 아닌지 검사 메소드
		ifTest();
	}
	@SuppressWarnings("deprecation")
	public void ifTest() {
		if (cnt >= 2) {
			score += 1;
			System.out.println("cnt_pass : " + cnt +"개 맞춤");
			ssingNext();
			t1.stop();
		} 
		else if (cnt < 2 ) {
			ssingNext_lose(cnt);
			System.out.println("cnt_fail : " + cnt);
			cancel();
		}	
	}
}