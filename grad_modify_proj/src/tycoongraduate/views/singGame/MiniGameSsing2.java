package tycoongraduate.views.singGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.Timer2;

public class MiniGameSsing2 extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private CharacterImage character = new CharacterImage();
	private ImageIcon sTest;

	private JTextField input;
	private JTextField input2;
	private JTextField input3;
	private PlayerVo player;
	
	String one = "";
	String two = "";
	String three = "";
	
	int num = (int) (Math.random() * 10) + 1;
	//노래 연습 게임 예제 이미지들 images/miniGame/sing_game/sTest_
	ImageIcon test = new ImageIcon("src/tycoongraduate/images/miniGame/sing_game/sTest_" + num + ".png");
	
	public MiniGameSsing2(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		JLabel label = new JLabel("빈칸에 알맞은 답은?");
		label.setFont(new Font("Neo둥근모", Font.PLAIN, 25));
		label.setBounds(70, 740, 250, 50);
		panel.add(label);
		input = new JTextField();
		input2 = new JTextField();
		input3 = new JTextField();

		input.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		input.setBounds(300, 740, 100, 50);

		input2.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		input2.setBounds(410, 740, 100, 50);

		input3.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		input3.setBounds(520, 740, 100, 50);

		panel.add(input);
		panel.add(input2);
		panel.add(input3);

		Timer2 timer = new Timer2(mf,this, player);
		Thread t1 = timer;
		//Timer2 클래스의 run()메소드가 실행되는 부분
		t1.start();
			
		MiniGameSsing_Test st = new MiniGameSsing_Test(mf, panel, num, input, input2, input3, player);
		input3.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				one = input.getText();
				System.out.println(one);

				two = input2.getText();
				System.out.println(two);

				three = input3.getText();
				System.out.println(three);
				
				String[] inputArr = {one, two, three};
				
				st.StestPass(inputArr);
				t1.stop();
			}
		});
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//노래연습 배경 이미지 - images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0, null); 
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1390, 250, null);
		if(player.getGender() == "M") {
			//추리닝 남자 동적이미지 = images/chracters/CharacterM.gif
			g.drawImage(character.characterMgifbf().getImage(), 50, 200, 398, 398, this);
		} else {
			//추리닝 여자 동적이미지 = images/chracters/CharacterW.gif
			g.drawImage(character.characterWgifbf().getImage(), 50, 200, 398, 398, this);
		}
		sTest = test; 
		g.drawImage(sTest.getImage(), 500, 180, 772, 408, null); 
		g.setFont(new Font("Neo둥근모", Font.BOLD, 36));
		g.drawString("빈칸에 알맞은 노래가사를 적자! 마지막 칸에 엔터를 치지 않으면 무효!", 70, 720);
		setOpaque(false);
	}
}