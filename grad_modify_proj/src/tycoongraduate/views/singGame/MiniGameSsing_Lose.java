package tycoongraduate.views.singGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.sosokMap.Sosok_singbutton;

public class MiniGameSsing_Lose extends JPanel{
	private MainFrame mf;
	private JPanel panel;
	private BackgroundImage bg = new BackgroundImage();
	private CharacterImage character = new CharacterImage();
	private DialogImage dialog = new DialogImage();
	private int score = 0;
	private int cnt;
	private PlayerVo player;

	public MiniGameSsing_Lose(MainFrame mf, PlayerVo player, int score, int cnt2) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.score = score;
		this.cnt = cnt2;
		this.setLayout(null);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("minigameLose Player : " + player);
				ChangePanel.changePanel(mf, panel, new Sosok_singbutton(mf, player));
				mf.revalidate();
			}
		});
	}
	public void jumsu(int jum) {
		int jum2 = jum;
		System.out.println("jum2 : " + jum2);
		JLabel jlabel = new JLabel(jum2+"뭐지 이점수는?");
		jlabel.setBounds(5,5,300,50);
		jlabel.setFont(new Font("Sanscerif",Font.BOLD,20));
		jlabel.setForeground(Color.BLACK);
		panel.add(jlabel);
	}
	public void paintComponent(Graphics g) {
		//소속사 블라인드 처리 된 배경 이미지 = images/companyMap/ingame_background_b.png
		g.drawImage(bg.singBgroundB().getImage(), 0, 0, null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1390, 250, null);
		//lose 크아 이미지 = images/miniGame/lose.png
		g.drawImage(dialog.loseDialog().getImage(),350,50,700,170,null);
		if(player.getGender() == "M" ) {
			//우는 남자아이 상반신 gif 이미지 = images/characters/Lose_CharacterM.gif
			g.drawImage(character.characterMgifcy().getImage(), 500, 250, 398, 398, this);
		} else {
			//우는 여자아이 상반신 gif 이미지 = images/characters/Lose_CharacterW.gif
			g.drawImage(character.characterWgifcy().getImage(), 500, 250, 398, 398, this);
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("3개중 "+cnt+"개 맞추셨어요. 다시 해보세요!", 70, 750);
		setOpaque(false);
		super.paintComponent(g);
	}
}