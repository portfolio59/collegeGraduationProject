package tycoongraduate.views.jungukGame;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import tycoongraduate.model.vo.game.JungukGameVo;
import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.sosokMap.Sosok_01_second;

public class JungukThirdPanel extends JPanel{
	private MainFrame mf;
	private JPanel panel;
	private PlayerVo player;
	private ButtonImage btnImg = new ButtonImage();
	private JungukGameVo gameImage = new JungukGameVo();
	private DialogImage dialog = new DialogImage();
	//시간초 변수
	private int t = 60;
	//정답 변수
	private String[] answer = new String[5];
	private int gamescore = 0;
	//유저한테 입력받을 텍스트 필드 변수
	private JTextField[] textFarr = new JTextField[5];
	//JTextFiled의 y좌표값을 일정하게 증가시켜줄 변수
	private int increNum = 550;
	private int earnPoint = 0;
	private Timer timer;

	public JungukThirdPanel(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		this.player = player;
		panel = this;
		this.setLayout(null);
		//0~4 까지의 난수발생으로 문제 랜덤으로 불러오기
		int i = (int)(Math.random()*5);
		System.out.println("난수 발생으로 문제번호는 : " + i);
		JLabel questLabel = new JLabel(new ImageIcon(gameImage.quest(i).getImage().getScaledInstance(1400, 900, 0)));
		questLabel.setLayout(null);
		questLabel.setBounds(0, -30, 1400, 900);
		//submit 버튼 이미지 = images/submit.png
		JButton submitBtn = new JButton(new ImageIcon(btnImg.submit_btn().getImage().getScaledInstance(190, 58, 0)));
		submitBtn.setLayout(null);
		submitBtn.setBounds(308, 710, 190, 38);
		submitBtn.setVisible(true);
		submitBtn.setBorderPainted(false);
		submitBtn.setContentAreaFilled(false);
		submitBtn.setFocusPainted(true);
		//지정된 구성 요소를 컨테이너에서 지정된 z-order인덱스로 이동하십시오. 
		//z 순서는 구성요소의 그리는 순서를 결정하며, z 순서가 가장 높은 구성요소는 먼저 그린다
		this.setComponentZOrder(submitBtn, 0);

		for(int j = 0 ; j < 5 ; j++) {
			textFarr[j] = new JTextField();
			textFarr[j].setLayout(null);
			textFarr[j].setBounds(130, increNum, 170, 30);
			increNum += 40;
		}
		//전국 노래자랑 실패 대화창 이미지 = images/miniGame/fail_dialog.png
		JLabel failLabel = new JLabel(new ImageIcon(dialog.jungukDialogFail().getImage().getScaledInstance(600, 200, 0)));
		failLabel.setLayout(null);
		failLabel.setBounds(440, 400, 600, 200);
		failLabel.setVisible(false);

		failLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new JungukFirstPanel(mf, player));
			}
		});
		Font neo = new Font("Sanscerif",Font.BOLD,27);
		JTextField tm = new JTextField("  남은 시간  - ");
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tm.setText("남은 시간  :  " + (t));
				tm.setToolTipText("여기는 시간을 나타냅니다");
				if (t == 0) {
					timer.stop();
					try {
						Thread.sleep(1200);
						failLabel.setVisible(true);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					tm.setText("작성후 꼭 버튼을 눌러주세요");
				}
				t--;
			}
		});
		tm.setFont(neo);
		tm.setLayout(null);
		tm.setBounds(250, 95, 250, 80);
		timer.start();
		questLabel.add(submitBtn);
		questLabel.add(failLabel);
		questLabel.add(tm);
		
		for(int j = 0; j < 5 ; j++) {
			this.add(textFarr[j]);
		}
		submitBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timer.stop();
				for(int j = 0; j < 5 ; j++) {
					//answer[j] = new String();
					answer[j] = textFarr[j].getText();
					System.out.println("textFarr["+j+"].getText() : " + textFarr[j].getText());
					System.out.println("answer["+j+"] : " + answer[j]);
				}

				if(i == 0) {
					for (int j = 0; j < answer.length; j++  ) {
						if("사랑의배터리".equals(answer[j])|| "사랑의 배터리".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("잘가라".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("산다는건".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("엄지척".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("부기맨".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						}
					}
				} else if(i == 1) {
					for (int j = 0; j < answer.length; j++  ) {
						if("태글을걸지마".equals(answer[j])|| "태클을 걸지마".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("안동역에서".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("보릿고개".equals(answer[j])|| "보릿 고개".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("동전인생".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("가지마".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						}
					}
				} else if(i == 2) {
					for (int j = 0; j < answer.length; j++  ) {
						if("사랑참".equals(answer[j])|| "사랑 참".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("어머나".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("첫사랑".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("짠짜라".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("이따이따요".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						}
					}
				} else if(i == 3) {
					for (int j = 0; j < answer.length; j++  ) {
						if("사랑의트위스트".equals(answer[j])|| "사랑의 트위스트".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("삼바의연인".equals(answer[j])|| "삼바의 연인".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("창밖의여인".equals(answer[j])||"창밖의 여인".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("갈매기사랑".equals(answer[j])||"갈매기 사랑".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("누이".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("그런여자없어요".equals(answer[j])||"그런여자 없어요".equals(answer[j])||"그런 여자 없어요".equals(answer[j]) ) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						}
					}
				} else if(i == 4) {
					for (int j = 0; j < answer.length; j++  ) {
						if("당신이좋아".equals(answer[j])|| "당신이 좋아".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("아모르파티".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("당신은".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("수은등".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						} if("빗속의여인".equals(answer[j])||"빗속의 여인".equals(answer[j])) {
							if(answer[j] != null) {
								earnPoint += 10;								
							}
						}
					}
				} if (earnPoint == 50) {
					earnPoint += 10;
				} if (earnPoint >= 0) {
					gamescore += earnPoint;
					System.out.println("전국gamescore : " + gamescore);
					player.setTotalScore(player.getTotalScore() + gamescore);
					player.setSingScore(player.getSingScore() + 1);
					player.setDanceScore(player.getDanceScore() + 1);
					//player 진행상태 이제 4가 되야함 
					player.setQuest(player.getQuest() + 1);
					System.out.println("전국노래자랑 게임 끝났을 때 player : " + player);
					questLabel.remove(submitBtn);
					questLabel.repaint();
					//전국 노래자랑 점 획득 글자 이미지 = images/miniGame/score_label.png
					JLabel scoreprint = new JLabel(
							new ImageIcon(dialog.jungukScoreLabel().getImage().getScaledInstance(600, 200, 0)));
					scoreprint.setLayout(null);
					scoreprint.setBounds(530, 300, 600, 200);
					questLabel.add(scoreprint);
					scoreprint.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							ChangePanel.changePanel(mf, panel, new Sosok_01_second(mf, player));
						}
					});
					switch(gamescore) {
					case 0: JLabel scoreP0 = 
							new JLabel(new ImageIcon(gameImage.time0().getImage().getScaledInstance(100, 200, 0)));
							scoreP0.setLayout(null);
							scoreP0.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP0);
							break;
					case 10: JLabel scoreP10 = 
							new JLabel(new ImageIcon(gameImage.time10().getImage().getScaledInstance(200, 200, 0))); 
							scoreP10.setLayout(null);
							scoreP10.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP10);
							break;
					case 20: JLabel scoreP20 = 
							new JLabel(new ImageIcon(gameImage.time20().getImage().getScaledInstance(200, 200, 0))); 
							scoreP20.setLayout(null);
							scoreP20.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP20);
							break;
					case 30: JLabel scoreP30 = 
							new JLabel(new ImageIcon(gameImage.time30().getImage().getScaledInstance(200, 200, 0)));
							scoreP30.setLayout(null);
							scoreP30.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP30);
							break;		
					case 40: JLabel scoreP40 = 
							new JLabel(new ImageIcon(gameImage.time40().getImage().getScaledInstance(200, 200, 0)));
							scoreP40.setLayout(null);
							scoreP40.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP40);
							break;
					case 50: JLabel scoreP50 = 
							new JLabel(new ImageIcon(gameImage.time50().getImage().getScaledInstance(200, 200, 0))); 
							scoreP50.setLayout(null);
							scoreP50.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP50);
							break;
					default: JLabel scoreP60 = 
							new JLabel(new ImageIcon(gameImage.time60().getImage().getScaledInstance(200, 200, 0))); 
							scoreP60.setLayout(null);
							scoreP60.setBounds(330, 300, 200, 200);
							questLabel.add(scoreP60);
							break;
					}
				}
			};
		});
		this.add(questLabel);
	}
};