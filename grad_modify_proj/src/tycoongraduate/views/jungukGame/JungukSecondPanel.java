package tycoongraduate.views.jungukGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class JungukSecondPanel extends JPanel {
   private MainFrame mf;
   private JPanel panel = this;
   private DialogImage dialog = new DialogImage();
   private PlayerVo player;
   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      //전국 노래자랑 이미지 있는 대화창 내용있음 = images/miniGame/junguk_game/junguk_dialog.PNG
      g.drawImage(dialog.jungukDialog2().getImage(), 0, 0, 1400, 900, null);
      setOpaque(false);
   }
   public JungukSecondPanel(MainFrame mf,PlayerVo player) {
	  this.mf = mf;
	  this.player = player;
      JLabel label = new JLabel() {
         public void paintComponent(Graphics g) {
        	//전국 노래자랑 문제 설명 이미지 = images/miniGame/junguk_game/Q1.png
            g.drawImage(dialog.jungukDialogQ1().getImage(), 0, -30, 1000, 600, null);
            setOpaque(false); 
            super.paintComponent(g);
         }
      };
      label.setLayout(null);
      label.setBounds(200,50,1000,700);
      this.add(label); 
      this.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent e) {
            System.out.println("전국노래자랑에서 세컨드페이지 들어옴!");
            ChangePanel.changePanel(mf, panel, new JungukThirdPanel(mf,player));
            mf.revalidate();
         }
      });
   }
}