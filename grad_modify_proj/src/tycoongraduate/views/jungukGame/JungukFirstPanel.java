package tycoongraduate.views.jungukGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class JungukFirstPanel extends JPanel {
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private PlayerVo player;
	private JLabel label;
	
	public JungukFirstPanel(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel =this;
		this.player = player;
		this.setLayout(null);
		if(player.getGender() == "M") {
			//츄리닝 남자 정적 이미지 = images/chracters/CharacterM.png
			label = new JLabel(new ImageIcon(character.characterMpngbf().getImage().getScaledInstance(250, 250, 0)));
		} else {
			//츄리닝 여자 정적 이미지 = images/chracters/CharacterW.png
			label = new JLabel(new ImageIcon(character.characterWpngbf().getImage().getScaledInstance(250, 250, 0)));
		}
		label.setBounds(350,220,270,300);
		this.add(label);
	}
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		//전국 노래자랑 배경 이미지 = images/miniGame/junguk_game/junguk_background.png
		g. drawImage(bg.jungukBground().getImage(), -300, 0, 1800, 750, null);
		//전국 노래자랑 이미지 없는 대화창 내용있음 = images/miniGame/junguk_game/mal_junguk1.PNG
		g. drawImage(dialog.jungukDialog1().getImage(),0,600,1400,250,null);
		setOpaque(false);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new JungukSecondPanel(mf,player));
				mf.revalidate();
			}
		});
	}
}