package tycoongraduate.views;

import javax.swing.JFrame;

public class MainFrame extends JFrame {
	public MainFrame(){
		//처음에 만들어질 창 사이즈를 가로 1400, 세로 900으로 맞추는 코드
		this.setBounds(60, 0, 1400, 900);
		GameStartPanel startPanel= new GameStartPanel(this);
		//다음에 이어질 화면인 GameStartPanel 클래스를 JFrame 에 붙여준다.
		this.add(startPanel);
		//창을 화면 정가운데로 위치시키도록 하는 코드
		this.setLocationRelativeTo(null);
		//창 사이즈 변경 불가능 하도록 하는 코드ㅌㅊ
		this.setResizable(false);
		//창이 보이도록 하는 설정
		this.setVisible(true);
		//창을 닫을 경우 이명령어를 써줘야 완전히 닫힌다
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}