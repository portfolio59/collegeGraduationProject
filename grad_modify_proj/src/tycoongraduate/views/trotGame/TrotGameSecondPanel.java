package tycoongraduate.views.trotGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.Border;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.ButtonImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.sosokMap.Sosok_01_trot;

public class TrotGameSecondPanel extends JPanel{
	private PlayerVo player;
	private ButtonImage btn = new ButtonImage();
	private DialogImage dialog = new DialogImage();
	private BackgroundImage bg = new BackgroundImage();
	//트롯스타게임 카메라 꺼진 (장식) 심사위원 이미지 = images/miniGame/camera_game/image_0.png
	private Image off = bg.trotOffCamera().getImage().getScaledInstance(400, 250,java.awt.Image.SCALE_SMOOTH);
	//트롯스타게임 카메라 켜진 (장식) 이미지 = images/miniGame/camera_game/image_1.png
	private Image on = bg.trotOnCamera().getImage().getScaledInstance(400, 250, java.awt.Image.SCALE_SMOOTH);
	
	private MainFrame mf;
	private JPanel panel;
	//화면에 보여질 9개의 카메라 생성 ( 이미지와 텍스트 집어넣을 예정 )
	private JLabel[] cameraoff = new JLabel[9];

	private int totalScore = 0 ;
	//남은 시간 고정변수 (30초로 지정)
	private int timeLeft = 30;
	//onHandler메소드를 이용해  화면에 보여질 9개의 카메라 중 ( 텍스트 집어넣을 고정변수 )
	private String con = "ON";
	private String coff = "OFF";

	private JLabel scoreDisplay;
	private JLabel timeLeftDisplay;
	//게임 끝나고 나서 몇점인지 띄워줄 예정
	private JLabel totalScoreLabel;
	//위에 변수에 부착할 거 ,이것을 누르면 다음 페이지로 넘어가게끔 구현S
	private JTextField printScoreText;
	
	private JButton startBtn;
	private Timer timer;
	
	//시작 버튼을 눌렀을 때 돌아가는 변수 ( T/F )
	private boolean programStart = false;

	public TrotGameSecondPanel(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		//초기 세팅
		initGUI();
		addCamera();
		initEvents();
	}
	private void initGUI() {
		this.setLayout(null);
		scoreDisplay = new JLabel("획득 점수 : 0");
		scoreDisplay.setHorizontalAlignment(SwingConstants.TRAILING);
		scoreDisplay.setForeground(Color.WHITE);
		scoreDisplay.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		scoreDisplay.setBounds(400,20,300,200);
		panel.add(scoreDisplay);
		//말풍선 틀 이미지 = images/dialog_format.png
		//getScaledInstance() 에서 3번째 인자는 이미지 크기 변환을 했을 때 이미지의 품질을 그대로 유지시켜준다
		totalScoreLabel = new JLabel(new ImageIcon(dialog.talkFormat().getImage().getScaledInstance(800, 400,java.awt.Image.SCALE_SMOOTH)));
		totalScoreLabel.setLayout(null);
		totalScoreLabel.setBounds(200, 0, 1000 , 1000);
		//처음에 안보이게 설정
		totalScoreLabel.setVisible(false);
		panel.add(totalScoreLabel);

		timeLeftDisplay = new JLabel("시간 : ");
		timeLeftDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		timeLeftDisplay.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		timeLeftDisplay.setForeground(Color.WHITE);
		timeLeftDisplay.setBounds(100,20,250,200);
		panel.add(timeLeftDisplay);

		//start 버튼 이미지 : images/start.png
		startBtn = new JButton(new ImageIcon(btn.start_btn().getImage().getScaledInstance(100, 50,java.awt.Image.SCALE_SMOOTH)));
		startBtn.setBounds(1100,60,110,33);
		startBtn.setBorderPainted(false);
		startBtn.setContentAreaFilled(false);
		startBtn.setFocusPainted(false);
		panel.add(startBtn);
	}
	private void addCamera() {
		cameraoff[0] = new JLabel(coff);
		cameraoff[0].setName(coff);
		cameraoff[0].setIcon(new ImageIcon(off));
		cameraoff[0].setBounds(50, 150, 400, 250);
		panel.add(cameraoff[0]);
		
		//라벨에 의해 보여질 텍스트 "off"
		cameraoff[1] = new JLabel(coff);
		//카메라off[]의 1번째 인덱스를 가지는 컴포넌트의 이름을 off로 지정
		cameraoff[1].setName(coff);
		cameraoff[1].setIcon(new ImageIcon(off));
		cameraoff[1].setBounds(470, 150, 400, 250);
		panel.add(cameraoff[1]);

		cameraoff[2] = new JLabel(coff);
		cameraoff[2].setName(coff);
		cameraoff[2].setIcon(new ImageIcon(off));
		cameraoff[2].setBounds(900, 150, 400, 250);
		panel.add(cameraoff[2]);

		cameraoff[3] = new JLabel(coff);
		cameraoff[3].setName(coff);
		cameraoff[3].setIcon(new ImageIcon(off));
		cameraoff[3].setBounds(50, 370, 400, 250);
		panel.add(cameraoff[3]);

		cameraoff[4] = new JLabel(coff);
		cameraoff[4].setName(coff);
		cameraoff[4].setIcon(new ImageIcon(off));
		cameraoff[4].setBounds(470, 370, 400, 250);
		panel.add(cameraoff[4]);

		cameraoff[5] = new JLabel(coff);
		cameraoff[5].setName(coff);
		cameraoff[5].setIcon(new ImageIcon(off));
		cameraoff[5].setBounds(900, 370, 400, 250);
		panel.add(cameraoff[5]);

		cameraoff[6] = new JLabel(coff);
		cameraoff[6].setName(coff);
		cameraoff[6].setIcon(new ImageIcon(off));
		cameraoff[6].setBounds(50, 600, 400, 250);
		panel.add(cameraoff[6]);

		cameraoff[7] = new JLabel(coff);
		cameraoff[7].setName(coff);
		cameraoff[7].setIcon(new ImageIcon(off));
		cameraoff[7].setBounds(470, 600, 400, 250);
		panel.add(cameraoff[7]);
		
		cameraoff[8] = new JLabel(coff);
		cameraoff[8].setName(coff);
		cameraoff[8].setIcon(new ImageIcon(off));
		cameraoff[8].setBounds(900, 600, 400, 250);
		panel.add(cameraoff[8]);
	}
	private void initEvents() {
		for(int i = 0; i < cameraoff.length; i++) {
			cameraoff[i].addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					JLabel cameralabel = (JLabel) e.getSource();
					String labelText = cameralabel.getText();
					pressedButton(labelText,cameralabel);			
				}
			});
		}
		startBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startBtn.setEnabled(false);
				timer.start();
				for(int i = 0; i < cameraoff.length;i++) {
					new Thread(new DThread(i)).start();
				}
			}
		});
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(timeLeft == 0) {
					timeLeftDisplay.setText("시간종료  : ");
					timer.stop();
					programStart = true;
					offAll();
					totalScoreLabel.setVisible(true);
					player.setTotalScore(player.getTotalScore() + totalScore);
					printScoreText = new JTextField(totalScore + "점을 획득하였습니다.") {
						@Override
						public void setBorder(Border boder) {
						}
					};
					printScoreText.setFont(new Font("Neo둥근모", Font.BOLD, 50));
					printScoreText.setForeground(Color.BLACK);
					printScoreText.setBounds(190,400,600,200);
					printScoreText.setEditable(false);
					printScoreText.setBackground(Color.white);
					printScoreText.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							ChangePanel.changePanel(mf, panel, new Sosok_01_trot(mf,player));
							mf.revalidate();
						}
					});
					totalScoreLabel.add(printScoreText);
				}
				timeLeftDisplay.setText("남은 시간 : " + timeLeft);
				timeLeft--;
			}
		});
	}
	private void pressedButton(String labelText, JLabel cameralabel) {
		String val = labelText;
		if(!(programStart)) {
			if("ON".equals(val)) {
				totalScore += 5;
				scoreDisplay.setText("획득 점수 : " + totalScore);
				cameralabel.setIcon(new ImageIcon(off));
				cameralabel.setText(coff);
				repaint();
			}else if("OFF".equals(val)){
				if(totalScore <= 0) {
					totalScore = 0;
					scoreDisplay.setText("획득 점수 : " + totalScore);
				} else {
					totalScore -= 5;
				}
			}
		}
		scoreDisplay.setText("획득 점수 : " + totalScore);
	}
	private void offAll() {
		if(programStart) {
			for(int i = 0; i < cameraoff.length; i++) {
				cameraoff[i].setIcon(new ImageIcon(off));
				cameraoff[i].setText(coff);
				repaint();
			}
		}
	}
	public class DThread implements Runnable{
		int index = 0; 
		public DThread(int index) {
			this.index = index;
		}
		@Override
		public void run() {
			while(!(programStart)) {
				try {
					//몇초뒤에 켜질 것인가 설정하는 랜덤변수 생성 (1초~9초)
					int ontime = new Random().nextInt(8000) + 1000;
					Thread.sleep(ontime);
					if(!(programStart)) {
						onHandler(index);
					}
					//잠시 쉬었다가
					Thread.sleep(3000);
					if(!(programStart)) {
						offHandler(index);
					}
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private void onHandler(int index) {
		cameraoff[index].setIcon(new ImageIcon(on));
		cameraoff[index].setText(con);
		repaint();
	}
	private void offHandler(int index) {
		cameraoff[index].setIcon(new ImageIcon(off));
		cameraoff[index].setText(coff);
		repaint();
	}
//	private void saveHighscore() {
//		BufferedWriter bw = null;
//		try {
//			bw = new BufferedWriter(new FileWriter(System.getProperty("user.dir")+"/highscore.txt", false));
//			bw.write(""+score);
//			bw.flush();
//			bw.close();
//		}catch(Exception e) {
//			JOptionPane.showMessageDialog(this, e.getMessage(), "Error while saving hiasda sd", JOptionPane.ERROR_MESSAGE);
//		}
//	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//트롯스타게임 빨간색 배경만 있는 이미지 = images/redCuttonBackground.jpeg
		g.drawImage(bg.redBground().getImage(), 0 , 0, 1400, 900, null);
		setOpaque(false);
		mf.repaint();
	}
}