package tycoongraduate.views.trotGame;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class TrotGameFirstPanel extends JPanel{
	private MainFrame mf;
	private TrotGameFirstPanel panel;
	private JLabel label;
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private PlayerVo player;
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//트롯스타게임 빨간색 배경에 심사위원 이미지 = images/miniGame/camera_game/panel3.png
		g.drawImage(bg.trotBground().getImage(), 0 , 0 ,1400,900,this);
		setOpaque(false);
	}
	public TrotGameFirstPanel(MainFrame mf, PlayerVo player) {
		System.out.println("TrotGameFirstPanel클래스에서 player의 춤스코어 : " + player.getDanceScore());
		System.out.println("TrotGameFirstPanel클래스에서 player의 노래스코어 : " + player.getSingScore());
		panel = this;
		this.mf = mf;
		this.player = player;
		//트롯스타게임 설명 이미지 = images/miniGame/camera_game/camera_game_dialog.png
		label = new JLabel(dialog.trotDialogLabel());
		panel.setLayout(null);
		label.setBounds(200,50,1000,700);
		panel.add(label);
		label.setVisible(false);
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				label.setVisible(true);
			}
		});
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new TrotGameSecondPanel(mf,player));
				mf.validate();
			}
		});
	}
}