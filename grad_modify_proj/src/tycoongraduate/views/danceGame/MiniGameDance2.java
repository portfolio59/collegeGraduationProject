package tycoongraduate.views.danceGame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class MiniGameDance2 extends JPanel  { 
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	//방향키 이미지들의 x좌표
	private int x = 300; 
	//방향키 이미지들의 x좌표 증가변수
	private int x2 = 300; 
	//방향키 이미지 붙일 라벨
	private JLabel[] arrows; 
	//이미지 파일명에 들어가는 숫자들
	private int[] arrNo; 
	private ArrayList<JLabel> inputs;
	//키보드 눌린 회수 저장하는 변수
	private int cnt = 0;
	//페이징 처리하는 변수
	private int index = 0;
	int num = 0;
	//게임 시작과 게임 끝을 알려주는 변수, 나중에 Player의 score에 포함예정
	int su = 0;
	//춤게임 점수 변수 14점 이상 넘어가야 성공
	public int point = 0;
	private PlayerVo player;
	public MiniGameDance2(MainFrame mf,PlayerVo player) { 
		this.mf=mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		//변수 초기화 설정
		arrows = new JLabel[7];   
		inputs = new ArrayList<JLabel>();
		arrNo = new int[7];

		buttonInit();
		mf.requestFocus();
		panel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(su !=1) {
					System.out.println(e.getKeyCode()+ " 미니게임댄스 su = 0일때");
					int num = 0;
					if(e.getKeyCode() == KeyEvent.VK_LEFT) {
						num = 4;
					}
					if(e.getKeyCode() == KeyEvent.VK_DOWN) {
						num = 3;
					}
					if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
						num = 2;
					}
					if(e.getKeyCode() == KeyEvent.VK_UP) {
						num = 1;
					}
					addButton(num);  
					mf.repaint();
					cnt++;
				}
			}
		});
		mf.add(panel);
		requestFocus();//이걸 추가하니까 panel에 리스너를 붙인게 먹힘
		this.setFocusable(true);
	}
	public void buttonInit(){
		x = 300; 
		for(int i = 0; i < arrows.length; ++i) {
			//1~4 까지의 범위인 숫자
			int num = new Random().nextInt(4) + 1;
			arrNo[i] = num;
			ImageIcon arrow = new ImageIcon("src/tycoongraduate/images/miniGame/dance_game/"+num+"-blue.png");
			arrows[i] = new JLabel(new ImageIcon(arrow.getImage().getScaledInstance(150, 150, 0)));
			arrows[i].setBounds(x, 100, 150, 150);
			x += 150;
			this.add(arrows[i]);
			System.out.println("arrows[" + i +"]의 x,y좌표 : " + arrows[i].getX() + ", " + arrows[i].getY());
		}
		this.repaint();
		//true 가 나옴
		System.out.println("mf.isFocused() : " + mf.isFocused());
	}
	public void addButton(int num) {
		System.out.println("addButton()에서 num의 값 :" + num);
		if(index >= 7){
			reset();
		}else{
			System.out.println("iudex7개 아래일때 : " + index);
			String color = "";
			if(arrNo[index] == num) {
				color = "blue";
				point += 1;
				System.out.println("point : " + point);
				if(point >=14) {
					su += 1; 
					System.out.println("su : " + su);
					keygamewin(); 
				}else {
					//방향키를 14개 이상 못 맞췄을 때
					//패널에 붙인 키리스너가 동작을 안하게 됨 = su = 1 이 되버리면서
					this.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							ChangePanel.changePanel(mf, panel, new MiniGameDance3_Lose(mf,player,point));
							mf.revalidate();
						}
					});
				}
				System.out.println("blue index : " + index);
			}
			else {
				color = "red";
				System.out.println("red index :" + index);
			}
			ImageIcon arrow = new ImageIcon("src/tycoongraduate/images/miniGame/dance_game/"+num+"-"+color+".png");
			JLabel input = new JLabel(new ImageIcon(arrow.getImage().getScaledInstance(150, 150, 0)));
			input.setBounds(x2, 350, 150, 150);
			inputs.add(input);
			x2 += 150;
			this.add(inputs.get(index));
			this.repaint();
			index++;
		}
	}
	public void reset(){
		if(cnt < 22) {
			for (int i = 0; i < arrows.length; i++) {
				this.remove(arrows[i]);
				System.out.println("arrows : " + arrows +" i "+ i);
				this.repaint();
			}
			buttonInit();
			for (int j = 0; j < arrNo.length; j++) {
				this.remove(this.inputs.get(j));
				this.repaint();
			}
			inputs.clear();
			mf.repaint();
			index = 0;
			x =300;
			x2 = 300;
		} else {
			System.out.println("end");
			ChangePanel.changePanel(mf, panel, new MiniGameDance3_Lose(mf,player,point));
		}
	}
	public void keygamewin() {
		MiniGameDance3_Win win = new MiniGameDance3_Win(mf,player); 
		win.keyjumsu(su);  
		player.setDanceScore(player.getDanceScore() + su);
		ChangePanel.changePanel(mf, panel, win);
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//소속사 배경 이미지 = images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); //0,600,1400,250
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("통과하면 춤 능력 점수 +1 !!!", 100, 700); 
		g.drawString("14개 이상 동일한 키보드를 맞춰야 합니다!", 100, 750);
		if(player.getGender() == "M") {
			//추리닝 남자아이  gif 이미지 = images/characters/CharacterM.gif
			g.drawImage(character.characterMgifbf().getImage(), 10, 100, 398, 398, this); // 398,398
		} else {
			//추리닝 여자아이  gif 이미지 = images/characters/CharacterW.gif
			g.drawImage(character.characterWgifbf().getImage(), 10, 100, 398, 398, this); // 398,398
		}
		setOpaque(false);
	}
}