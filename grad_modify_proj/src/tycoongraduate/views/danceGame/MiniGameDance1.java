package tycoongraduate.views.danceGame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;

public class MiniGameDance1 extends JPanel { 
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private MiniGameDance1 panel = this;
	private MainFrame mf;
	private PlayerVo player;
	public MiniGameDance1(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		this.addMouseListener(new MyMouseAdapter()); 
	}
	public void paintComponent(Graphics g) {
		//소속사 배경 이미지 = images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0, this.getWidth(),this.getHeight(),null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 0, 600, 1400, 250,null); // 0,600,1400,250
		if(player.getGender() == "M") {
			//추리닝 남자아이  gif 이미지 = images/characters/CharacterM.gif
			g.drawImage(character.characterMgifbf().getImage(), 520, 200, 398, 398, this);// 398,398
		} else {
			//추리닝 여자아이  gif 이미지 = images/characters/CharacterW.gif
			g.drawImage(character.characterWgifbf().getImage(), 520, 200, 398, 398, this);// 398,398
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 30));
		g.drawString("오늘도 춤연습을 열심히 해보자~", 100, 710); // 660 -> 710
		g.drawString("화면에 방향키가 나오면 시간안에 따라 쳐보자!", 100, 760);  //710->760
//		g.drawString("Yeah i can do this well WHOO ~ wow  ", 100, 760);  
//		g.drawString("They call it like ping pong ~ ho ", 100, 810); 
		setOpaque(false);
		super.paintComponent(g);
	}
	class MyMouseAdapter extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent e) {
			ChangePanel.changePanel(mf, panel, new MiniGameDance2(mf,player));      
			mf.revalidate();
		}
	}
}