package tycoongraduate.views.danceGame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import tycoongraduate.model.vo.game.PlayerVo;
import tycoongraduate.model.vo.image.BackgroundImage;
import tycoongraduate.model.vo.image.CharacterImage;
import tycoongraduate.model.vo.image.DialogImage;
import tycoongraduate.views.MainFrame;
import tycoongraduate.views.common.ChangePanel;
import tycoongraduate.views.sosokMap.Sosok_junguk;

public class MiniGameDance3_Win extends JPanel{ 
	private MainFrame mf;
	private JPanel panel;
	private CharacterImage character = new CharacterImage();
	private BackgroundImage bg = new BackgroundImage();
	private DialogImage dialog = new DialogImage();
	private int su;
	private PlayerVo player;
	
	public MiniGameDance3_Win(MainFrame mf, PlayerVo player) {
		this.mf = mf;
		panel = this;
		this.player = player;
		this.setLayout(null);
		this.setFocusable(true);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ChangePanel.changePanel(mf, panel, new Sosok_junguk(mf,player));
				mf.revalidate();
			}
		});
	}
	public void keyjumsu(int su) {
		this.su = su;
		System.out.println("MinigameDance3클래스에서 keyjumsu()메소드에서의 su : " + su);
		JLabel jlabel = new JLabel("춤 능력이 "+su+"점 올랐습니다!");
		jlabel.setBounds(5,5,300,50);
		jlabel.setFont(new Font("Sanscerif",Font.BOLD,20));
		panel.add(jlabel);
	}
	public void paintComponent(Graphics g) {
		//소속사 배경 이미지 = images/companyMap/ingame_background.png
		g.drawImage(bg.singBground().getImage(), 0, 0,1400,900, null);
		//말풍선 틀 이미지 = images/dialog_format.png
		g.drawImage(dialog.talkFormat().getImage(), 5, 620, 1400, 250, null);
		//win 크아 이미지 = images/miniGame/lose.png
		g.drawImage(dialog.winDialog().getImage(),350,50,700,170,null);
		if(player.getGender() == "M") {
			//추리닝 남자아이  gif 이미지 = images/characters/CharacterM.gif
			g.drawImage(character.characterMgifbf().getImage(), 500, 250, 398, 398, this);
		} else {
			//추리닝 여자아이  gif 이미지 = images/characters/CharacterW.gif
			g.drawImage(character.characterWgifbf().getImage(), 500, 250, 398, 398, this);
		}
		g.setFont(new Font("Neo둥근모", Font.BOLD, 40));
		g.drawString("오예~! 춤 능력치가 올랐다!!", 70, 750);
		setOpaque(false);
		super.paintComponent(g);
	}
}